package edu.uw.bothell.css.dsl.mass.apps.ChinesePostman;
import java.io.*;

public class Args2Agents implements Serializable {
    public Args2Agents( int[] itinerary, int[][] footprints, int distance, int total ) {
	this.itinerary = itinerary;
	this.footprints = footprints;
	this.distance = distance;
	this.total = total;
    }
    public int[] itinerary = null;
    public int[][] footprints = null;
    public int distance = 0;
    public int total = 0;
}
