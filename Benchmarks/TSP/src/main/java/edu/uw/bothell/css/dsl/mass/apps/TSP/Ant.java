package edu.uw.bothell.css.dsl.mass.apps.TSP;
import edu.uw.bothell.css.dsl.MASS.*;
import java.io.*;
import java.util.*;

public class Ant extends Agent implements Serializable {
    // function identifiers
    public static final int setup_ = 0;
    public static final int moveAnts_ = 1;
    public static final int updateTrails_ = 2;
    public static final int getTourLength_ = 3;
    public static final int getTour_ = 4;

    public Object callMethod( int functionId, Object argument ) {
        switch( functionId ) {
        case setup_:         return setup( argument );
	case moveAnts_:      return moveAnts( argument );  
	case updateTrails_:  return updateTrails( argument );
	case getTourLength_: return getTourLength( argument );
	case getTour_:       return getTour( argument );
        }
        return null;
    }

    // private data
    private double alpha = 1; // trail preference
    private double beta = 5;  // greedy preference
    private double pr = 0.01; // probability of pure random selection of the next town
    private double Q = 500;   // new trail deposit coefficient; 
    private int n = 0;        // # cities
    private int newIndex = 0; // where it is going to visit
    private Random rand = null;
    private int[] tour = null;        // itinerary 
    private boolean[] visited = null; // check if it has visited the i-th city
    private double probs[] = null;    // a collection of probability for going to a city
    private double tourLength = 0.0;  // the total length of the current tour

    public Ant( ) {
	super( );
    }

    public Ant( Object arg ) {
	n = ( ( Integer )arg ).intValue( ); // n = # cities
	tour = new int[n];
	visited = new boolean[n];
	probs = new double[n];
    }

    public Object setup( Object arg ) {
	// for debugging: just print out its id
	MASS.getLogger( ).debug( "agent(" + getAgentId( ) + ") is set up at place[" + getPlace( ).getIndex( )[0] + "]" );

	// each ant has its own random generator
	if ( rand == null )
	    rand = new Random( getAgentId( ) );

	// clear newIndex.
	newIndex = 0;

	// clear visited[]
	clear( );

	// choose a starting city
	visitTown( rand.nextInt( n ) );
	
	return arg;
    }

    public Object moveAnts( Object arg ) {
	if ( newIndex == n ) {    // visited all cities
	    visitTown( tour[0] ); // go home.
	    newIndex = 0;         // reset to tour[0]
	}
	else
	    visitTown( selectNextTown( ) );
	return null;
    }

    public Object updateTrails( Object arg ) {
	double contribution = Q / tourLength;                        // compute my contribution
	double[] trails = ( ( City )getPlace( ) ).trails;            // retrieve this city's trails
	++newIndex;
	// put my pheromon from the current to the next (newIndex) city
	trails[ ( newIndex == n ) ? tour[0] : tour[newIndex] ] += contribution;  
	migrate( ( newIndex == n ) ? tour[0] : tour[newIndex] );

	return null;
    }

    public Object getTourLength( Object arg ) {
	MASS.getLogger().debug( "agent[" + getAgentId( ) + "] returns tourLength = " + tourLength );
	return new Double( tourLength );
    }

    public Object getTour( Object arg ) {
	return tour;
    }

    // Given an ant select the next town based on the probabilities                                                
    // we assign to each town. With pr probability chooses                                                         
    // totally randomly (taking into account tabu list).  
    private int selectNextTown( ) {
        // sometimes just randomly select                                                                          
	int nextTown = -1;
        if (rand.nextDouble() < pr) {
            int t = rand.nextInt(n - newIndex); // random town
	    if ( ( nextTown = selectNthTown( t ) ) != -1 ) {
		MASS.getLogger( ).debug( "agent[" + getAgentId( ) + "] chose " + nextTown + " randomly." );
		return nextTown;
	    }
        }
        // calculate probabilities for each town (stored in probs)                                                 
        probTo( );
        // randomly select according to probs                                                                      
	double r = rand.nextDouble();
        double tot = 0;
	for (int i = 0; i < n; i++) {
            tot += probs[i];
            if (tot >= r) {
		MASS.getLogger( ).debug( "agent[" + getAgentId( ) + "] chose " + i + " with probs = " + r );
		return i;
	    }
        }
	if ( ( nextTown = selectNthTown( 0 ) ) != -1 ) {
	    MASS.getLogger( ).debug( "agent[" + getAgentId( ) + "] chose " + nextTown + 
                                     " as the 1st available city, because tot = " + tot + " < r = " + r );
	    return nextTown;
	}
        MASS.getLogger( ).debug("agent[" + getAgentId( ) + "]: Not supposed to get here" );
	MASS.getLogger( ).debug( "newIndex = " + newIndex );
	for ( int i = 0; i < newIndex; i++ )
	    MASS.getLogger( ).debug( "tour[" +i + "] = " + tour[i] );
	for ( int i = 0; i < n; i++ )
	    MASS.getLogger( ).debug( "visited(" +i + ") = " + visited(i) );

        throw new RuntimeException("agent[" + getAgentId( ) + "]: Not supposed to get here" );
    }

    private int selectNthTown( int nth ) {
	int j = -1;
	for (int i = 0; i < n; i++) {
	    if (!visited(i))
		j++;
	    if (j == nth )
		return i;
	}
	return -1; // otherwise
    }

    // Approximate power function, Math.pow is quite slow and we don't need accuracy.                              
    // See:                                                                                                        
    // http://martin.ankerl.com/2007/10/04/optimized-pow-approximation-for-java-and-c-c/                           
    // Important facts:                                                                                            
    // - >25 times faster                                                                                          
    // - Extreme cases can lead to error of 25% - but usually less.                                                
    // - Does not harm results -- not surprising for a stochastic algorithm.                                       
    private static double pow(final double a, final double b) {
        final int x = (int) (Double.doubleToLongBits(a) >> 32);
        final int y = (int) (b * (x - 1072632447) + 1072632447);
        return Double.longBitsToDouble(((long) y) << 32);
    }

    // Store in probs array the probability of moving to each town                                                 
    // [1] describes how these are calculated.                                                                     
    // In short: ants like to follow stronger and shorter trails more.                                             
    private void probTo( ) {
	double[] distances = ( ( City )getPlace( ) ).distances;
	double[] trails = ( ( City )getPlace( ) ).trails;

        double denom = 0.0;
        for (int l = 0; l < n; l++)
            if (!visited(l))
                denom += pow(trails[l], alpha)
                    * pow(1.0 / distances[l], beta);

        for (int j = 0; j < n; j++) {
            if (visited(j)) {
                probs[j] = 0.0;
            } else {
                double numerator = pow(trails[j], alpha)
                    * pow(1.0 / distances[j], beta);
                probs[j] = numerator / denom;
            }
        }
    }

    private void visitTown( int town ) {
	if ( newIndex < n ) { // en route to my destination
	    tour[newIndex++] = town; // write this town in my tour
	    visited[town] = true;    // check I visited this town
	}
	else
	    ;                 // returning to my home, (town)

	if ( newIndex > 0 ) { // now en route to my destination
	    double[] distances = ( ( City )getPlace( ) ).distances;
	    tourLength += distances[town];
	}
	MASS.getLogger( ).debug( "agent(" + this + ") [" + getAgentId( ) + "] will move from " 
                                 + getPlace( ).getIndex( )[0] + " to " + town );
	migrate( town );
    }

    private boolean visited(int i) {
	return visited[i];
    }

    private void clear( ) {
	for ( int i = 0; i < n; i++ )
	    visited[i] = false;
	tourLength = 0.0;
    }
}
