package edu.uw.bothell.css.dsl.mass.apps.TSP;
import edu.uw.bothell.css.dsl.MASS.*;
import java.io.*;
import java.util.*;

public class City extends Place {
    // function identifiers
    public static final int init_ = 0;
    public static final int clear_ = 1;
    public static final int evapolate_ = 2;
 
    public Object callMethod( int functionId, Object argument ) {
        switch( functionId ) {
        case init_:                return init( argument );
	case clear_:               return clear( argument );
	case evapolate_:           return evapolate( argument );
         }
        return null;
    }

    // my local variables
    private double c = 1.0;             // original amount of trail
    private double evaporation = 0.5;   // trail evaporation coefficient
    public double[] distances = null;   // accessed by local agents
    public double[] trails = null;      // accessed by local agents

    public City( Object arg ) {
    }

    /**
     * @param arg ( double[] )distances, each recording a distance to a different city
     * @return null
     */
    private Object init( Object arg ) {
	// maintain a distance to a different city
	distances = ( double[] )arg;

	// for debugging: print the distance from this city to all the others
	MASS.getLogger( ).debug( "city[" + getIndex( )[0] + "]: distances.length = " + distances.length );
	for ( int i = 0; i < distances.length; i++ )
	    MASS.getLogger( ).debug( "city[" + getIndex( )[0] + "] to [" + i + "] = " + distances[i] );

	// clear a pheromon to each city with c (= 1.0)
	clear( null );
	return null;
    }

    /**
     * @param arg nothing
     * @return null
     */
    private Object clear( Object arg ) {
	// create a trails to maintain pheromons
	if ( trails == null )
	    trails = new double[ distances.length ];

	// pheromon to each city initialized with c (= 1.0)
	for( int i = 0; i < trails.length; i++ )
	    trails[i] = c;
	return null;
    }

    private Object evapolate( Object arg ) {
	// evaporation                                                                                             
        for (int i = 0; i < trails.length; i++)
	    trails[i] *= evaporation;
	return null;
    }
}
