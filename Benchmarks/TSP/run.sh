#!/bin/bash
# $1 filename
# $2 # computing nodes
# $3 # threads
cd target
java -Xms1g -Xmx8g -jar TSP-1.0-SNAPSHOT-jar-with-dependencies.jar ../$1 $2 $3
