package edu.uw.bothell.css.dsl.mass.apps.BreadthFirst;
import java.util.*;

public class GraphTraverse {
    static boolean[] visited = null;
    
    static boolean stats = false;
    static int nHops = 0;
    static int nBacktracks = -1; // don't count a backtrack from the source to main( )
    static int nBranches = 0;
    
    public static void main( String[] args ) {

	// Read and validate input parameters
	if ( args.length < 2 ) {
	    System.err.println( "args = " + args.length +
				" should be 2 or 3: #vertices source [y]" );
	    System.exit( -1 );
	}
	int nNodes = Integer.parseInt( args[0] );
	int src = Integer.parseInt( args[1] );
	if ( nNodes < 1 || src >= nNodes ) {
	    System.err.println( "Nodes(" + nNodes + "), src(" + src +
				") should be > 0 and < nNodes respectively.");
	    System.exit( -1 );
	}
	stats = ( args.length == 3 );
	System.out.println( "Nnodes = " + nNodes + " src = " + src +
			    " stats = " + ( stats ? "yes" : "no" ) );

	// Create a map
	Map[] map = new Map[nNodes];
	visited = new boolean[nNodes];
        for ( int i = 0; i < nNodes; i++ ) {
            // System.out.println( "Node " + i + ":" );
            map[i] = new Map( nNodes, i );
	    visited[i] = false;

	    /*
            for ( int j = 0; j < map[i].neighbors.length; j++ ) {
                System.out.println( "to " + map[i].neighbors[j]
                                    + " with distance "
                                    + map[i].distances[j] );
            }
	    System.out.println( );
	    */
        }

	// Time measurement starts
	System.out.println( "Go!" );
	long startTime = System.currentTimeMillis( );
	
	// Start GraphTraverse
	if ( map[src].neighbors.length > 0 )
	    nBranches++; // at least one branch from the source
	traverse( map, src );

	// Time measurement ends
	long elapsedTime = System.currentTimeMillis( ) - startTime;
	System.out.println( "Elapsed time = " + elapsedTime );

	// Print out statistics for each step
	if ( stats )
	    System.out.println( "# hops = " + nHops +
				", # branches = " + nBranches +
				", # backtracks = " + nBacktracks );
    }

    private static void traverse( Map[] map, int current ) {
	// visited a new vertex
	visited[current] = true;
	// System.out.println( "visited " + current );

	// explore each neighbor of it.
	int branches = 0;
	for ( int i = 0; i < map[current].neighbors.length; i++ ) {
	    if ( visited[map[current].neighbors[i]] == false ) {
		nHops++;
		branches++;
		traverse( map, map[current].neighbors[i] );
	    }
	}
	nBranches += ( branches > 1 ) ? ( branches - 1 ) : 0;
	nBacktracks++; 
    }
}
