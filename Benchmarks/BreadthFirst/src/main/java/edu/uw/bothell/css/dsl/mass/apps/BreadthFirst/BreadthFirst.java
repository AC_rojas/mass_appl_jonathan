package edu.uw.bothell.css.dsl.mass.apps.BreadthFirst;
import edu.uw.bothell.css.dsl.MASS.*;
import edu.uw.bothell.css.dsl.MASS.logging.LogLevel;
import java.util.*;

/**
 * BreadthFirst.java - a breadth first search by agents
 * @author  Munehiro Fukuda <mfukuda@uw.edu>
 * @version 1.0
 * @since   Nov. 15, 2017
 */
public class BreadthFirst {
    /**
     * Traverse all graph vertices with agents from vertex[0].
     * @param args TBD
     */
    public static void main( String[] args ) {

	int statSteps = 0;
	int statAgents = 0;

	// Read and validate input parameters
	if ( args.length != 4 ) {
            System.err.println( "args = " + args.length + " should be 4:" +
				" nNodes, source, nProcs, nThrs" );
            System.exit( -1 );
        }
        int nNodes = Integer.parseInt( args[0] );
        int src = Integer.parseInt( args[1] );
        if ( nNodes < 1 || src >= nNodes ) {
            System.err.println( "Nodes(" + nNodes + ") and src(" + src +
                                ") should be > 0, < nNodes, and < nNodes respectively." );
            System.exit( -1 );
        }

	// Set up arguments to MASS.init( );
	String[] arguments = new String[4];
	arguments[0] = "dslab";
	arguments[1] = "ignored";
	arguments[2] = "machinefile.txt";
	arguments[3] = "12345";
	int nProc = Integer.parseInt( args[2] );
	int nThr = Integer.parseInt( args[3] );
	System.out.println( "nNodes = " + nNodes + " src = " + src +
			    " nProc = " + nProc + " nThr = " + nThr );
	MASS.setLoggingLevel( LogLevel.ERROR );
	MASS.init( arguments, nProc, nThr );

	// Create a map
	Places network = new Places( 1, Node.class.getName( ), null, nNodes );
	network.callAll( Node.init_ );

	// Time measurement starts
	System.out.println( "Go!" );
	long startTime = System.currentTimeMillis( );
	
	// Instantiate the very first crawler agent.
	Args2Agents args2agents = new Args2Agents( src );
	Agents crawler = new Agents( 2, Crawler.class.getName( ), ( Object )args2agents,
				     network, 1 );

	for ( int time = 0; crawler.nAgents( ) > 0; time++ ) {
	    statSteps++; // for statistics
	    // System.out.println( "*** time = " + time + "************" );

	    // STEP 1: have each agent migrate along a different edge 
	    // emanating from the current node
	    // at time == 0, the very first agent hops to the origin
	    // System.out.println( "... step 1: migration .................." );
	    crawler.callAll( Crawler.departure_ );
	    crawler.manageAll( );
	    
	    // STEP2: have each agent spawn its children if a new node 
	    // hasn't been visited yet.
	    // System.out.println( "... step 2: spawn ......................" );
	    statAgents += crawler.nAgents( ); // for statistics
	    
	    crawler.callAll( Crawler.onArrival_ );
	    crawler.manageAll( );

	    // System.out.println( "nAgents = " + crawler.nAgents( ) );
	}

	// Time measurement ends
	long elapsedTime = System.currentTimeMillis( ) - startTime;
	System.out.println( "Elapsed time = " + elapsedTime );

	// Stats
	System.out.println( "Statistics: #steps = " + statSteps +
			    " #agents = " + statAgents );
	
	MASS.finish( );
    }
}
