#!/bin/bash
# $1 graph node size
# $2 src node id
# $3 # computing nodes
# $4 # threads
cd target
java -Xms1g -Xmx8g -jar BreadthFirst-1.0-SNAPSHOT-jar-with-dependencies.jar $1 $2 $3 $4
