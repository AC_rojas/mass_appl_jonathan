package edu.uw.bothell.css.dsl.mass.apps.TrianglesDoAll;
import java.util.*;

public class SequentialCount {
    static int nTriangles = 0;
    
    static boolean stats = false;
    static int nHops = 0;
    static int nBacktracks = -1; // don't count a backtrack from the source to main( )
    static int nBranches = 0;
    
    public static void main( String[] args ) {

	// Read and validate input parameters
	if ( args.length < 1 ) {
	    System.err.println( "args = " + args.length +
				" should be 1 or 2: #vertices [y]" );
	    System.exit( -1 );
	}
	int nNodes = Integer.parseInt( args[0] );
	if ( nNodes < 1 ) {
	    System.err.println( "Nodes(" + nNodes +
				") should be > 0 and < nNodes respectively.");
	    System.exit( -1 );
	}
	stats = ( args.length == 2 );
	System.out.println( "Nnodes = " + nNodes + 
			    " stats = " + ( stats ? "yes" : "no" ) );

	// Create a map
	Map[] map = new Map[nNodes];
        for ( int i = 0; i < nNodes; i++ ) {
            // System.out.println( "Node " + i + ":" );
            map[i] = new Map( nNodes, i );

	    /*
            for ( int j = 0; j < map[i].neighbors.length; j++ ) {
                System.out.println( "to " + map[i].neighbors[j]
                                    + " with distance "
                                    + map[i].distances[j] );
            }
	    System.out.println( );
	    */
        }

	// Time measurement starts
	System.out.println( "Go!" );
	long startTime = System.currentTimeMillis( );
	
	// Start SequentialCount
	if ( map[map.length - 1].neighbors.length > 0 )
	    nBranches++; // at least one branch from the source  
	for ( int i = map.length - 1; i >= 0; i-- )
	    step1( map, i );

	System.out.println( "# triangles = " + nTriangles );
	
	// Time measurement ends
	long elapsedTime = System.currentTimeMillis( ) - startTime;
	System.out.println( "Elapsed time = " + elapsedTime );

	// Print out statistics for each step
	if ( stats )
	    System.out.println( "# hops = " + nHops +
				", # branches = " + nBranches +
				", # backtracks = " + nBacktracks );
    }

    private static void step1( Map[] map, int current ) {
	boolean multi = false;
	for ( int i = 0; i < map[current].neighbors.length; i++ ) {
	    int neighbor = map[current].neighbors[i];
	    if ( neighbor < current ) {
		if ( multi )
		    nBranches++;
		else
		    multi = true;
		nHops++;
		
		step2( map, current, neighbor );
	    }
	}
    }

    private static void step2( Map[] map, int source, int current ) {
	boolean multi = false;
	for ( int i = 0; i < map[current].neighbors.length; i++ ) {
	    int neighbor = map[current].neighbors[i];
	    if ( neighbor < current ) {
		if ( multi )
		    nBranches++;
		else
		    multi = true;
		nHops++;
		    
		step3( map, source, current, neighbor );
	    }
	}
	nBacktracks++;
    }

    private static void step3( Map[] map, int source, int intermediate, int current ) {
	for ( int i = 0; i < map[current].neighbors.length; i++ ) {
	    int neighbor = map[current].neighbors[i];
	    if ( neighbor == source ) {
		// found a triangle
		if ( stats )
		    System.out.println( "triangle: " + source + " - " + intermediate + " - " + current );
		nTriangles++;
		nBacktracks++;
		return;
	    }
	}
	nBacktracks++;
    }
}
