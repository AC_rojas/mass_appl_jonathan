#!/bin/bash
# $1 graph node size
# $2 # computing nodes
# $3 # threads
# $4 [y/n] show
cd target
java -Xms1g -Xmx8g -jar Triangles-1.0-SNAPSHOT-jar-with-dependencies.jar $1 $2 $3 $4
cd classes
java -Xms1g -Xmx8g edu.uw.bothell.css.dsl.mass.apps.Triangles.SequentialCount $1 $4
