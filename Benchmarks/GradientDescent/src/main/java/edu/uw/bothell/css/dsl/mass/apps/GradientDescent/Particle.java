package edu.uw.bothell.css.dsl.mass.apps.GradientDescent;
import edu.uw.bothell.css.dsl.MASS.*;
import edu.uw.bothell.css.dsl.MASS.logging.LogLevel;
import java.io.*;
import java.util.*;

public class Particle extends Agent implements Serializable {
    // function identifiers
    public static final int init_      = 1;
    public static final int touchNgo_  = 2;

    public Object callMethod( int functionId, Object argument ) {
	switch( functionId ) {
	case init_:     return init( argument );
	case touchNgo_: return touchNgo( argument );
	}
	return null;
    }

    // private data
    private int nextX = 0;
    private int nextY = 0;
    private boolean firstGo = true;

    /**
     * Is the default constructor.
     */
    public Particle( ) {
	super( );
    }

    /**
     * Is the actual constructor.
     * @param arg should be null.
     */
    public Particle( Object arg ) {
	// nothing to do
	
	MASS.getLogger( ).debug( "agent(" + getAgentId( ) + ") was born." );
    }

    public Object init( Object arg ) {
	// go to the initial location
	int[] initValues = (int [])arg;
	go( initValues[0], initValues[1] );

	return null;
    }

    public Object touchNgo( Object arg ) {
	// Receive the globably best-known coordinates and value
	BestXYV bestData = ( BestXYV )arg;
	bestData.x = getPlace( ).getIndex( )[0];
	bestData.y = getPlace( ).getIndex( )[1];
	bestData.v = ( ( SinCos )getPlace( ) ).value;

	findLowestNeighbor( );
	go( nextX, nextY );

	return bestData;
    }

    private void go( int x, int y ) {
	migrate( x, y );
	if ( firstGo )
	    MASS.getLogger( ).debug( "agent(" + getAgentId( ) + ") at value(" + 
				     ") will move to [" + x + ", " + y + "]" );
	else {
	    MASS.getLogger( ).debug( "agent(" + getAgentId( ) + ") at value(" + 
				     ( ( SinCos )getPlace( ) ).value + 
				     ") will move to [" + x + ", " + y + "]" );
	    firstGo = false;
	}
    }

    private void findLowestNeighbor( ) {
	// initialize the next lowest coordinate and value to the current
	int lowestX = getPlace( ).getIndex( )[0], lowestY = getPlace( ).getIndex( )[1];
	double lowestV = ( ( SinCos )getPlace( ) ).value;

	// search the lowest neighbor
	int candX = 0, candY = 0;
	for ( int i = -1; i <= 1; i++ ) {
	    if ( ( candX = getPlace( ).getIndex( )[0] + i ) < 0 )
		continue;
	    for ( int j = -1; j <= 1; j++ ) {
		if ( ( candY = getPlace( ).getIndex( )[1] + j ) < 0 )
		    continue;
		double neighbor = ( ( SinCos )getPlace( ) ).getValue( i, j );
		if ( neighbor < lowestV ) {
		    lowestX = candX;
		    lowestY = candY;
		    lowestV = neighbor;
		}
	    }
	}

	// set the next destination
	nextX = lowestX;
	nextY = lowestY;
    }
}
