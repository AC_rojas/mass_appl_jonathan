package edu.uw.bothell.css.dsl.mass.apps.KNN;
import java.util.*;

public class DataGen {
    public static void main( String[] args ) {
	double lowerX = Double.parseDouble( args[0] );
	double upperX = Double.parseDouble( args[1] );
	double lowerY = Double.parseDouble( args[2] );
	double upperY = Double.parseDouble( args[3] );
	int datasize = Integer.parseInt( args[4] );
	int k = Integer.parseInt( args[5] );
	double sizeX = upperX - lowerX;
	double sizeY = upperY - lowerY;

	System.err.println( "lowerX = " + lowerX + ", upperX = " + upperX +
			    ", lowerY = " + lowerY + ", upperY = " + upperY +
			    ", datasize = " + datasize + ", k = " + k +
			    ", sizeX = " + sizeX + ", sizeY = " + sizeY );

	Random rand = new Random( 1 );

	for ( int i = 0; i < k; i++ ) {
	    // Generate centroid i
	    double[] centroid = new double[3];
	    centroid[0] = rand.nextDouble( ) * sizeX + lowerX;
	    centroid[1] = rand.nextDouble( ) * sizeY + lowerY;
	    centroid[2] = i;

	    System.err.println( "centroid[" + i + "] = " + centroid[0] + " " + centroid[1] + " " + centroid[2] );
	    
	    // Generate the (datasize / k) of random data items around i
	    int stride = datasize / k;
	    int lower = stride * i + ( ( i < datasize % k ) ? i : ( datasize % k ) );
	    int upper = lower + stride + ( ( i < datasize % k ) ? 1 : 0 );
	    System.err.println( lower + " through to " + ( upper - 1 ) );

	    for ( int j = lower; j < upper; j++ ) {
	    
		double x = 0;
		double y = 0;
		double attr = i;
		do {
		    double meanX = centroid[0];
		    double devX = sizeX / 8;
		    
		    x = gaussian( meanX, devX );
		} while( x < lowerX || upperY < x );
		
		do {
		    double meanY = centroid[1];
		    double devY = sizeY / 8;
		    
		    y = gaussian( meanY, devY );
		} while( y < lowerY || upperY < y );
		
		System.out.println( x + " " + y + " " + attr );
	    }
	}

	/*
	double mean = 100.0;
	System.out.println( "Poisson ********" );
	for ( int i = 0; i < 50; i++ )
	    System.out.println( getPoissonRandom( mean ) );
	System.out.println( "Gaussian *******" );
	for ( int i = 0; i < 50; i++ )
	    System.out.println( gaussian( mean/4, mean ) );
	*/
    }

    private static double gaussian( double mean, double deviation ) {
	Random r = new Random( );
	return r.nextGaussian( ) * deviation + mean;
    }

    private static int getPoissonRandom(double mean) {
	Random r = new Random();
	double L = Math.exp(-mean);
	int k = 0;
	double p = 1.0;
	do {
	    p = p * r.nextDouble();
	    k++;
	} while (p > L);
	return k - 1;
    }

}
