package edu.uw.bothell.css.dsl.mass.apps.KNN;
import edu.uw.bothell.css.dsl.MASS.*;
import edu.uw.bothell.css.dsl.MASS.logging.LogLevel;
import java.util.*;

/**                                                                                                                                  
 * KNN - an agent-based k-nearest neighbors
 * @author  Munehiro Fukuda <mfukuda@uw.edu>                                                                                         
 * @version 1.0                                                                                                                      
 * @since   November 4, 2017
 */
public class KNN {
    /**
     *
     */
    public static void main( String[] args ) {
	// Read and validate input parameters
	if ( args.length != 10 ) {
	    System.err.println( "args = " + args.length + " should be 10:" +
				"lowerX, upperX, lowerY, upperY, placesize, datasize, k, filename, nProcs, nThrs" );
	    System.exit( -1 );
	}
	double[] bound = new double[4];
	bound[0] = Double.parseDouble( args[0] );
	bound[1] = Double.parseDouble( args[1] );
	bound[2] = Double.parseDouble( args[2] );
	bound[3] = Double.parseDouble( args[3] );

	int placesize = Integer.parseInt( args[4] );
	int datasize = Integer.parseInt( args[5] );
	int k = Integer.parseInt( args[6] );
	String filename = args[7];

	if ( bound[0] >= bound[1] || bound[2] >= bound[3] || placesize <= 0 || datasize <= 0 ) {
	    System.err.println( "Check these parameters: bound[0] < bound[1]? bound[2] < bound[3]? placesize > 0? datasize > 0?" );
	    System.err.println( "bound[]" + bound[0] + "bound[]" + bound[1] + "bound[]" + bound[2] + "bound[]" + bound[3] +
				"placesize" + placesize + "datasize" + datasize );
	    System.exit( -1 );
	}

	// Set up arguments to MASS.init( );
	String[] arguments = new String[4];
	arguments[0] = "dslab";
	arguments[1] = "ignored";
	arguments[2] = "machinefile.txt";
	arguments[3] = "12345";
	int nProc = Integer.parseInt( args[8] );
	int nThr = Integer.parseInt( args[9] );


	System.out.println( "bound[0] = " + bound[0] + ", bound[1] = " + bound[1] +
			   ", bound[2] = " + bound[2] + ", bound[3] = " + bound[3] +
			   ", placesize = " + placesize + ", datasize = " + datasize +
			   ", nProc = " + nProc + ", nThr = " + nThr ); 
	MASS.setLoggingLevel( LogLevel.ERROR );
	MASS.init( arguments, nProc, nThr );

	// Create a space
	Args2Places args2places = new Args2Places( bound, filename, datasize, k );
	Places space = new Places( 1, Space.class.getName( ), 1, args2places, placesize, placesize );
	space.callAll( Space.init_ );

	// Read a query data
	double[] query = new double[2];
	Scanner input = new Scanner( System.in );
	System.out.println( "Inpu a query data:" );
	System.out.print( "x = " );
	query[0] = input.nextDouble( );
	System.out.print( "y = " );
	query[1] = input.nextDouble( );
	space.callAll( Space.query_, query );

	// Time measurement starts
	System.out.println( "Go!" );
	long startTime = System.currentTimeMillis( );

	// Populate a report agent at each space
	Object arg = new Integer( Reporter.isParent );
	Agents reporters = new Agents( 2, Reporter.class.getName( ), arg, space, placesize * placesize );

	// Disseminate a ripple from a query data
	for ( int i = 0; i < placesize; i++ ) {
	    space.callAll( Space.detect_ );            // Ripple dissemination
	    space.callAll( Space.propagate_ );
	    space.exchangeBoundary( );           
	    reporters.callAll( Reporter.spawnChild_ ); // A report child creation
	    reporters.manageAll( );
	    if ( reporters.nAgents( ) >= k + placesize * placesize )
		break;                                 // Got an enough # reporters
	}

	reporters.callAll( Reporter.killAdults_ );      // Kill all the original parent agents
	reporters.manageAll( );
	System.out.println( "nAgents = " + reporters.nAgents( ) );	

	// Prepare dumm arguments to agents in order to receive their return values
	int nAgents = reporters.nAgents( );
	int[][] dummyArgs = new int[nAgents][1];
	Object[] candidateData = ( Object[] )reporters.callAll( Reporter.reportData_, ( Object[] )dummyArgs );
	
	ArrayList<double[]> finalData = new ArrayList<double[]>( );
	for ( int i = 0; i < nAgents; i++ )
	    finalData.addAll( ( ArrayList<double[]> )candidateData[i] );

	// double[0]:x, double[1]:y, double[2]:attribute, double[3]:distance
	// Sort my data
	Collections.sort( finalData, new Comparator<double[]>( ) {
		@Override
		public int compare( double[] data1, double[] data2 ) {
		    if ( data1[3] < data2[3] ) return -1;
		    if ( data1[3] == data2[3] ) return 1;
		    return 1;
		}
	    } );

        // Time measurement ends
	long elapsedTime = System.currentTimeMillis( ) - startTime;
	System.out.println( "Elapsed time = " + elapsedTime );

	int max = Math.min( k, finalData.size( ) );
	for ( int i = 0; i < k; i++ ) {
	    double[] eachData = finalData.get( i );
	    System.out.println( "x = " + eachData[0] + ", y = " + eachData[1] + ", attr = " + eachData[2] + ", distance = " + eachData[3] );
	}

	MASS.finish( );
    }
}
