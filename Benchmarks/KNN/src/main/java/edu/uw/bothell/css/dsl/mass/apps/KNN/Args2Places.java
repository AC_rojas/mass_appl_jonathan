package edu.uw.bothell.css.dsl.mass.apps.KNN;
import java.io.*;

public class Args2Places implements Serializable {
    public double[] bound = null;
    public String filename = null;
    public int datasize = 0;
    public int k = 0;

    public Args2Places( double[] bound, String filename, int datasize, int k ) {
	this.bound = bound;
	this.filename = filename;
	this.datasize = datasize;
	this.k = k;
    }
}
