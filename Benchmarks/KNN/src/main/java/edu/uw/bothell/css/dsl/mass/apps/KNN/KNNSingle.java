package edu.uw.bothell.css.dsl.mass.apps.KNN;
import java.io.*;
import java.util.*;

public class KNNSingle {
    // data set
    public static String filename = null;
    public static int datasize = 0;
    public static FileInputStream file = null;
    public static ArrayList<double[]> mydata = new ArrayList<double[]>( ); // a list of data 
    
    public static void main( String[] args ) {
	// Read and validate input parameters
	if ( args.length != 3 ) {
	    System.err.println( "args = " + args.length + " should be 3:" +
				"datasize, k, filename" );
	    System.exit( -1 );
	}

	int datasize = Integer.parseInt( args[0] );
	int k = Integer.parseInt( args[1] );
	String filename = args[2];

	if (  datasize <= 0 ) {
	    System.err.println( "datasize" + datasize );
	    System.exit( -1 );
	}

	System.out.println( ", datasize = " + datasize +
			    ", k = " + k +
			    ", filename = " + filename );

	// Read a data file
	try {
	    file = new FileInputStream( filename );
	} catch( Exception e ) { e.printStackTrace( ); };
	Scanner scanner = new Scanner( file );

	// read the data ( x, y, attribute )
	for ( int i = 0; i < datasize; i++ ) {
	    if ( !scanner.hasNextDouble( ) )
		break;
	    double[] dataset = new double[4];
	    dataset[0] = scanner.nextDouble( ); // x
	    dataset[1] = scanner.nextDouble( ); // y
	    dataset[2] = scanner.nextDouble( ); // attribute
	    dataset[3] = Double.MAX_VALUE;      // distance
	    mydata.add( dataset );
	}

	// Read a query data
	double[] query = new double[2];
	Scanner input = new Scanner( System.in );
	System.out.println( "Inpu a query data:" );
	System.out.print( "x = " );
	query[0] = input.nextDouble( );
	System.out.print( "y = " );
	query[1] = input.nextDouble( );

	// Time measurement starts
	System.out.println( "Go!" );
	long startTime = System.currentTimeMillis( );

        // Compute all distances
	for ( int i = 0; i < mydata.size( ); i++ ) {
	    double[] dataset = mydata.get( i );
	    dataset[3] = computeDistance( dataset[0], dataset[1], query[0], query[1] );
	    //System.out.println( "x = " + mydata.get( i )[0] + ", y = " + mydata.get( i )[1] +
	    //				", attr = " + mydata.get( i )[2] + ", dist = " + mydata.get( i )[3] );
	}

	// Sort them out
	Collections.sort( mydata, new Comparator<double[]>( ) {
		                    @Override
				    public int compare( double[] data1, double[] data2 ) {
					if ( data1[3] < data2[3] ) return -1;
					if ( data1[3] == data2[3] ) return 0;
					return 1;
					//return ( int )( data1[3] - data2[3] );
				    }
	    } );

	// Time measurement ends
	long elapsedTime = System.currentTimeMillis( ) - startTime;
	System.out.println( "Elapsed time = " + elapsedTime );

	int max = Math.min( k, mydata.size( ) );
	for ( int i = 0; i < max; i++ ) {
	    double[] eachData = mydata.get( i );
	    System.out.println( "x = " + eachData[0] + ", y = " + eachData[1] + ", attr = " + eachData[2] + ", distance = " + eachData[3] );
	}
    }

    /**
     * Computes the distnace between a given training data (d) and a query data (s)
     */
    private static double computeDistance( double dx, double dy, double sx, double sy ) {
	return Math.sqrt( ( dx - sx ) * (dx - sx ) + ( dy - sy ) * ( dy - sy ) );
    }
    
}
