/*

 	MASS Java Software License
	© 2012-2017 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:      

	© 2012-2017 University of Washington. MASS was developed by Computing and Software Systems at University of 
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.mass.apps.climateanalysis.toe;

import edu.uw.bothell.css.dsl.MASS.Agents;
import edu.uw.bothell.css.dsl.MASS.Places;
import edu.uw.bothell.css.mass.apps.climateanalysis.ProvAdapter;
import edu.uw.bothell.css.mass.apps.climateanalysis.climatemodels.ClimateModelInterface;

/**
 *
 * @author jwoodrin
 */
public abstract class AbstractToe implements ToeInterface{

    public int jobNumber;
    
    int x;
    int y;
    int z;
    
    public int numOfYears;
    
    Places places; // our data grid
    Agents agents;   
    
    ClimateModelInterface inputClimateModel;
    
    private ProvAdapter provLogger;
    
    /**
     * 
     * 
     * @param args
     * @param numProc
     * @param numThr 
     */
    public void setArgs(ClimateModelInterface inputModel, int jobNum, ProvAdapter provLog){
        inputClimateModel = inputModel;
        jobNumber =  jobNum;    
        provLogger = provLog;
    }
    
    abstract void massInit();
    
    public int getNumToeYears(){
        return numOfYears;
    }
    
    @Override
    public abstract void executeCalculations();
    
    public abstract void placesTest();

    public abstract void agentTest();

    /**
     * @return the provLogger
     */
    public ProvAdapter getProvLogger() {
        return provLogger;
    }
}
