/*

 	MASS Java Software License
	© 2012-2015 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:      

	© 2012-2015 University of Washington. MASS was developed by Computing and Software Systems at University of 
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.dsl.MASS.FluteParallel;

import java.util.Date;
import java.util.Random;

import edu.uw.bothell.css.dsl.MASS.MASS;
import edu.uw.bothell.css.dsl.MASS.Places;

/**
 *
 * @author amala
 */
public class FluteParallelSimulation {

    /**
     * @param args the command line arguments
     */

    public static void main( String[] args ) throws Exception {

    	// TODO code application logic here
        // verify arguments
        System.out.println("Total arguments = " + args.length);
        if ( args.length != 5 ) {
            System.out.println( "usage: java -cp MASS.jar:jsch-0.1.44.jar:. FluteSimulation userid port nProcs nThrs size" );
            System.exit( -1 );
        }
        
        String[] massArgs = new String[4];
        massArgs[0] = args[0];          // user name
        massArgs[2] = "machinefile.txt"; // machine file
        massArgs[3] = args[1];           // port

        int nProcesses = Integer.parseInt( args[2] );
        int nThreads = Integer.parseInt( args[3] );
        int PopulationPerCommunity = Integer.parseInt ( args[4] );

	// Inputs for the Simulation
	int Communities = 7;
	int InitialSeeds = 10;
	int SimulationDays = 180;
	Random rn = new Random();
	
        // start MASS
        MASS.init( massArgs, nProcesses, nThreads );

        //create the adjacency matrix
        Places infectedPeople = new Places( 1, "InfectedPeople", ( Object )null, Communities, PopulationPerCommunity );
	
	//initialize the infected people to false
        infectedPeople.callAll( InfectedPeople.init_, ( Object[] )null );

	// Infect the initial seeds
        for( int i = 0 ; i < InitialSeeds ; i++ )
	{
	    int randomCommunity = rn.nextInt( Communities );
	    int randomPerson = rn.nextInt( PopulationPerCommunity );
	    int[] index = { randomCommunity , randomPerson };

	    //update the Place to true to indicate its infected	    
//	    infectedPeople.callSome( InfectedPeople.initialInfection_, ( Object )null, index[0], index[1] );
	    infectedPeople.callAll( InfectedPeople.initialInfection_, ( Object[] )null, index[0], index[1] );
	}

	// Collect back the place to find the infected seeds 	
	Object[] temp = new Object[ Communities * PopulationPerCommunity ];
	Object[] TotalInfected = (Object[]) infectedPeople.callAll( InfectedPeople.collectInfected_, temp );
				    
	//Start the timer
	Date startTime = new Date( );
	
	//Run the simulation for given number of days
	for ( int day = 1 ; day < SimulationDays + 1 ; day++ )
	{
	    //Infect People at Day time
	    TotalInfected = (Object[]) infectedPeople.callAll( InfectedPeople.collectInfected_, temp );
	    infectedPeople.callAll( InfectedPeople.day_, ( Object ) TotalInfected );
	    
	    //Infect People at Night time
	    TotalInfected = (Object[]) infectedPeople.callAll( InfectedPeople.collectInfected_, temp );
	    infectedPeople.callAll( InfectedPeople.night_, ( Object ) TotalInfected );

	    //Exchange the migrants
	    /*int migrated = 0;
	    while ( migrated < MigrantWorkers )
	    {
		int randomCommunity = rn.nextInt( Communities );
                int randomPerson = rn.nextInt( PopulationPerCommunity );
                int[] index = { randomCommunity , randomPerson };
		infectedPeople.exchangeSome( InfectedPeople.response_, ( Object )check, index[0], index[1] );

	     }*/
	    
	    //Find total infected people
	    TotalInfected = (Object[]) infectedPeople.callAll( InfectedPeople.collectInfected_, temp );
	   
	    int totalInfections = 0 ;
	    for ( int i = 0 ; i < TotalInfected.length ; i++ )
	    {
		//System.out.print ( "\t" + ( int )( Object ) TotalInfected[i] );
		if ( ( int )( Object ) TotalInfected[i]  == 1)
	        {
		    totalInfections++;
		}
		    
	    }
	    //   System.out.println( "After Day" + day + " total infected are: " + totalInfections);

	    //Vaccinate people - Response
            int vaccinated = 0;
            while ( vaccinated < ( int )( totalInfections/8 ))
	    {
		    int randomCommunity = rn.nextInt( Communities );
		    int randomPerson = rn.nextInt( PopulationPerCommunity );
		    int[] index = { randomCommunity , randomPerson };

		    //update the Place to true to indicate its infected
		    //Object check = new Object();
		    //Object isVaccinated =
		    infectedPeople.callAll( InfectedPeople.response_, ( Object[] )null, index[0], index[1] );
		    vaccinated++ ;
	     }
	    
	}
	
	Date endTime = new Date( );
        System.out.println( "\tTime (ms): " + ( endTime.getTime( ) - startTime.getTime( ) ) );
	
	//Finish MASS
        MASS.finish();
	
    }
    
}