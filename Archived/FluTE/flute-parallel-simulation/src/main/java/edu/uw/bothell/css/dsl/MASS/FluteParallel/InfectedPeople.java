/*

 	MASS Java Software License
	© 2012-2015 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:      

	© 2012-2015 University of Washington. MASS was developed by Computing and Software Systems at University of 
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.dsl.MASS.FluteParallel;

import edu.uw.bothell.css.dsl.MASS.Place;

/**
 *
 * @author amala
 */

public class InfectedPeople extends Place {

	public static final int init_ = 0;
	public static final int initialInfection_ = 1;
	public static final int day_ = 2;
	public static final int night_ = 3;

	public static final int sync_ = 4;
	public static final int response_ = 5;
	public static final int collectInfected_ = 7;

	int isInfected;

	int community = getSize()[0];

	int populationPerCommunity = getSize()[1];
	int data;

	// checking all four neighbors if infected: north, east, south, and west
	// private final int north = 0, east = 1, south = 2, west = 3;
	int[] neighbour = new int[4];
	private int sizeX, sizeY;
	private int myX, myY;

	public InfectedPeople() {
		super();
	}

	public InfectedPeople(Object args) {
		super();
	}

	public Object callMethod(int funcId, Object args) {

		switch (funcId) {
		case init_:
			return init(args);
		case initialInfection_:
			return initialInfection(args);
		case collectInfected_:
			return (Object) collectInfected(args);
		case day_:
			return day(args);
		case night_:
			return night(args);
		case sync_:
			return sync(args);
		case response_:
			return response(args);
		}

		return null;

	}

	public Object collectInfected(Object args) {
		return (Object) isInfected;
	}

	public Object day(Object args_TotalInfected) {

		int west = Math.max(myX - 1, 0);
		int east = Math.min(myX + 1, sizeX - 1);
		int south = Math.max(myY - 1, 0);
		int north = Math.min(myY + 1, sizeY - 1);

		neighbour[0] = west * sizeY + myY;
		neighbour[1] = east * sizeY + myY;
		neighbour[2] = myX * sizeY + south;
		neighbour[3] = myX * sizeY + north;

		// If i am not infected then check all 4 neighbor
		// even if they are in different communities
		if (isInfected != 1) {

			for (int i = 0; i < 4; i++) {
			
				// If neighbor is infected, then infect me
				if (neighbour[i] == 1) {
					isInfected = 1;
				}

			}
		
		}

		return null;
	
	}

	/*
	 * Initialize all the places to false, to indicate that no one is infected
	 */
	public Object init(Object args) {

		isInfected = 0;
		data = 0;
		sizeX = getSize()[0];
		sizeY = getSize()[1]; // size is the base data members
		myX = getIndex()[0];
		myY = getIndex()[1]; // index is the base data members
		
		return null;
	
	}

	/*
	 * Update the place to true, to indicate that these people are initially
	 * infected
	 */
	public Object initialInfection(Object args) {

		isInfected = 1;
		data = 1;
	
		return null;
	
	}

	public Object night(Object args) {
		
		int west = Math.max(myX - 1, 0);
		int east = Math.min(myX + 1, sizeX - 1);

		neighbour[0] = west * sizeY + myY;
		neighbour[1] = east * sizeY + myY;

		// If i am not infected then check neighbor in my community
		// because at night i am with people in my community only
		if (isInfected != 1) {
			
			for (int i = 0; i < 2; i++) {
				
				// If neighbor is infected, then infect me
				if (neighbour[i] == 1) {
					isInfected = 1;
					break;
				}
			
			}
		
		}
		
		return null;
	
	}

	public Object response(Object args) {

		boolean infectMe;
		
		if (isInfected == 1)
			infectMe = false;
		else {
			isInfected = 1;
			infectMe = true;
		}
	
		// TODO - shouldn't this method return something?
		// return infectMe;
		return null;
	
	}

	public Object sync(Object args) {
		// isInfected = 1;
		return null;
	}

}