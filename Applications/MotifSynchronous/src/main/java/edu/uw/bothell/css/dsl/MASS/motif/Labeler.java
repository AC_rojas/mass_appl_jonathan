/*

 	MASS Java Software License
	© 2012-2015 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:      

	© 2012-2015 University of Washington. MASS was developed by Computing and Software Systems at University of 
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.dsl.MASS.motif;

// Labeler.java
//
// by Matt Kipps
// 12/12/14

import java.io.*;
import java.util.*;

public class Labeler {

    // labelg program options
    private static final String programName = "./labelg";
    private static final int invariant = 3;
    private static final int mininvarlevel = 1;
    private static final int maxinvarlevel = 100;

    // file options
    private static final String filePrefix  = ".";
    private static final String filePostfix = ".g6";

    // data members
    private String inputFilename;
    private String outputFilename;
    private String[] args;

    public Labeler() {
        long currentTime = System.currentTimeMillis();
        this.inputFilename = filePrefix + "rawgraph6_" +
                             currentTime + filePostfix;
        this.outputFilename = filePrefix + "canonical_" +
                              currentTime + filePostfix;
        this.args = getArgs();
    }

    private String[] getArgs() {
        String[] args = {
            programName,
            "-i" + invariant,
            "-I" + mininvarlevel + ":" + maxinvarlevel,
            inputFilename,
            outputFilename};
        return args;
    }

    public Map<String, Integer> getCanonicalLabels(
        Map<String, Integer> subgraphs) {
        // Get canonical labels using the labelg program.
        // This function communicates with labelg using input and output files.
        //
        // code adapted from Vartika Verma's Nemo Finder project (UWB 2014)

        BufferedWriter writer = null;
        BufferedReader inputReader = null;
        BufferedReader outputReader = null;
        Map<String, Integer> labels = new HashMap<String, Integer>();
        int returnCode = 0;
        try {
            writer = new BufferedWriter(new FileWriter(inputFilename));
            for (String graph:subgraphs.keySet()) {
                writer.write(graph);
                writer.write('\n');
            }
            writer.close();
            Process p = Runtime.getRuntime().exec("chmod 777 args");
            p.waitFor();
            Process labelg = Runtime.getRuntime().exec(args);

            // close output stream, input stream & error stream
            labelg.getOutputStream().close();
            closeInputStream(labelg.getInputStream());
            closeInputStream(labelg.getErrorStream());

            // wait for labelg to complete execution
            returnCode = labelg.waitFor();

            // read back in the input and output file
            inputReader = new BufferedReader(new FileReader(inputFilename));
            outputReader = new BufferedReader(new FileReader(outputFilename));
            String inputLine = inputReader.readLine();
            String outputLine = outputReader.readLine();

            // combine the input and output, assuming labelg writes output
            // in the same order as the input is provided
            while (outputLine != null) {
                int count = subgraphs.get(inputLine);
                if (labels.containsKey(outputLine)) {
                    count += labels.get(outputLine);
                }
                labels.put(outputLine, count);
                inputLine = inputReader.readLine();
                outputLine = outputReader.readLine();
            }
            inputReader.close();
            outputReader.close();
        } catch (Exception e) {
            System.err.println("Exception " + e + " raised");
            e.printStackTrace();
            labels = null;
        } finally {
            try { writer.close(); } catch (Exception e) {}
            try { inputReader.close(); } catch (Exception e) {}
            try { outputReader.close(); } catch (Exception e) {}
            deleteFile(inputFilename);
            deleteFile(outputFilename);
        }

        if (labels == null) {
            System.exit(-1);
        }

        if (returnCode != 0) {
            System.err.println("`labelg` exited with a return code of: " +
                returnCode);
            System.err.print("`labelg` executed with: ");
            for (String line:args) {
                System.err.print(line + " ");
            }
            System.err.println();
            System.exit(-1);
        }

        return labels;
    }

    private void closeInputStream(InputStream stream) throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(stream));
        String l = r.readLine();
        while (l != null) {
            l = r.readLine();
        }
        r.close();
    }

    private void deleteFile(String filename) {
        try {
            File f = new File(filename);
            f.delete();
        } catch (Exception e) {
            System.err.println("Exception " + e +
                " raised when attempting to delete " + filename);
        }
    }

}
