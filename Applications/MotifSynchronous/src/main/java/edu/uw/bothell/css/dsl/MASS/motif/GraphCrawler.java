/*

 	MASS Java Software License
	© 2012-2015 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:      

	© 2012-2015 University of Washington. MASS was developed by Computing and Software Systems at University of 
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.dsl.MASS.motif;

// GraphCrawler.java
//
// by Matt Kipps
// 12/12/14

import java.io.Serializable;

import edu.uw.bothell.css.dsl.MASS.*;

@SuppressWarnings("serial")
public class GraphCrawler extends Agent {

    public static final int update_ = 0;
    public Object callMethod(int functionID, Object args) {
        switch(functionID) {
            case update_:
                update();
                break;
            default:
                break;
        }
        return null;
    }

    private int migrateTo;
    private Subgraph subgraph;
    private AdjacencyList extension;

    public GraphCrawler(Object rawArgs) {
        super();

        Constructor constructor = (Constructor)rawArgs;
        this.migrateTo = constructor.getMigrateTo();
        this.subgraph  = constructor.getSubgraph();
        this.extension = constructor.getExtension();
    }

    public void update() {
        if (migrateTo != -1) {
            migrate(migrateTo);
            migrateTo = -1;
            return;
        }

        // 'v' is equal to the subgraph root, unless it is empty, then
        // 'v' is equal to 'w' ('w' is the current node).
        GraphNode node = getNode();
        int w = node.getFirstIndex();
        int v;
        if (subgraph.size() == 0) {
            v = w;
        } else {
            v = subgraph.root();
        }

        // add the current node to the subgraph
        if (subgraph.size() == subgraph.order() - 1) {
            subgraph.add(w, node.getAdjacencyList());
            node.addToSubgraphs(subgraph);
            kill();
            return;
        }

        // examine each node 'u' from the set of nodes adjacent to 'w',
        // and add it to the next extension if it is exclusive to the
        // subgraph, and greater than 'v'
        CompactHashSet.Iter uIter = node.getAdjacencyList().iterator();
        while (uIter.hasNext()) {
            int u = uIter.next();
            if (u > v) {
                if (subgraph.excludes(u)) {
                    extension.add(u);
                }
            }
        }

        if (extension.isEmpty()) {
            kill();
            return;
        }

        // add the current node to the subgraph
        subgraph.add(w, node.getAdjacencyList());

        // extend the subgraph
        CompactHashSet.Iter iter = extension.iterator();
        if (extension.size() > 1) {
            Object[] spawnParams = new Object[extension.size() - 1];
            for (int i = 0; i < spawnParams.length; i++) {
                int spawnAt = iter.next();
                iter.remove();
                spawnParams[i] = (Object)(new GraphCrawler.Constructor(
                    spawnAt,
                    subgraph.copy(),
                    (subgraph.size() < subgraph.order() - 1) ? extension.copy() : null));
            }

            spawn(spawnParams.length, spawnParams);
        }

        // pick the destination for this crawler
        int destination = iter.next();
        iter.remove();

        // migrate to the new destination
        migrate(destination);
    }

    private GraphNode getNode() {
        return (GraphNode)getPlace();
    }

    // the nested Constructor class simplifies the instantiation of GraphCrawler
    // objects through MASS library calls
    public static class Constructor implements Serializable {
        private int migrateTo;
        private int motifSize;
        private Subgraph subgraph;
        private AdjacencyList extension;

        public Constructor(int motifSize) {
            this(-1, motifSize, null, null);
        }

        public Constructor(
            int migrateTo,
            Subgraph subgraph,
            AdjacencyList extension) {
            this(migrateTo, subgraph.order(), subgraph, extension);
        }

        private Constructor(
            int migrateTo,
            int motifSize,
            Subgraph subgraph,
            AdjacencyList extension) {
            this.migrateTo = migrateTo;
            this.motifSize = motifSize;
            this.subgraph  = subgraph;
            this.extension = extension;
        }


        public int getMigrateTo() {
            return migrateTo;
        }

        public Subgraph getSubgraph() {
            if (subgraph == null) {
                return new Subgraph(motifSize);
            } else {
                return subgraph;
            }
        }

        public AdjacencyList getExtension() {
            if (extension == null) {
                return new AdjacencyList();
            } else {
                return extension;
            }
        }
    }
}
