import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;


import java.io.IOException;
import java.util.*;
import java.util.List;

import static java.util.Collections.sort;

/**
 * RangeSearchMapReduce.java
 * Project: RangeSearch
 * University of Washington Bothell, Distributed Systems Laboratory
 * Spring 2020
 * @author Satine Paronyan
 */

public class RangeSearchMapReduce {
    private final static int CORES_PER_NODE = 3;
    private final static int NUMBER_OF_NODES = 4;
    private static int slice = CORES_PER_NODE * NUMBER_OF_NODES;

    // arguments received in main()
    private static int xCoordinateMin = 0;
    private static int xCoordinateMax = 0;
    private static int Xmin = 0;
    private static int Xmax = 0;
    private static int Ymin = 0;
    private static int Ymax = 0;


    public static class MapLocalKDTrees extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> {

        JobConf conf;
        public void configure(JobConf job) { this.conf = job; }

        @Override
        public void map(LongWritable key, Text value, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {

            int size = Integer.parseInt(conf.get("size"));
            int chunk = size / slice;

            // contains the x ranges for each chunk
            int[][] ranges = new int[slice][2];
            /*for (int i = 0; i < slice; i++) {
                ranges[i][0] = i;
                ranges[i][1] = i + chunk;
            } */

            int rangeCount = (xCoordinateMax - xCoordinateMin) / slice;
            int prev = xCoordinateMin;
            for (int i = 0; i < slice; i++) {
                ranges[i][0] = prev;
                ranges[i][1] = prev + rangeCount;
                prev += rangeCount;
            }
            ranges[slice - 1][1] = xCoordinateMax + 1;

            /*System.out.println("Printing ranges array:");
            for (int i = 0; i < slice; i++) {
                System.out.println(ranges[i][0] + " - " + ranges[i][1]);
            }*/

            // read lines and tokenize with space " "
            String line = value.toString();
            StringTokenizer tokenizer = new StringTokenizer(line, " ");

            //System.out.println("\nPrinting output collect:");
            // Read all the points and send in sorted order to reduce.
            // Each reducer receives its portion of points sliced by x-coordinate.
            while (tokenizer.hasMoreTokens()) {
                String point = tokenizer.nextToken(); //x,y
                int xVal = Integer.parseInt(point.substring(0, point.indexOf(",")));

                for (int i = 0; i < slice; i++) {
                    //System.out.println("For i equals : " + i);

                    if (xVal >= ranges[i][0] && xVal <= ranges[i][1]) {
                        output.collect(new Text(Integer.toString(i)), new Text(point));
                        //System.out.println("Output collect: " + i + " " + point);
                        break;
                    }

                }
            }

        }
    } // end of MapLocalKDTrees


    public static class MapResults extends MapReduceBase implements Mapper<Text, Text, Text, Text> {
        JobConf conf;

        public void configure(JobConf job) {
            this.conf = job;
        }

        @Override
        public void map(Text key, Text value, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
            // read lines and tokenize with space " "
            String line = value.toString();
            StringTokenizer tokenizer = new StringTokenizer(line, " ");

            while (tokenizer.hasMoreTokens()) {
                String point = tokenizer.nextToken(); //x,y
                output.collect( new Text("master"), new Text(point));
            }
        }
    }


    public static class ReduceLocalKDTrees extends MapReduceBase implements Reducer<Text, Text, Text, Text> {

        @Override
        public void reduce(Text key, Iterator<Text> values, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
            List<Point2D> pointsList = new ArrayList<>();

            while (values.hasNext()) {
                String value = values.next().toString();
                String[] xy = value.split(",");
                Point2D p = new Point2D(Integer.parseInt(xy[0]), Integer.parseInt(xy[1]));
                pointsList.add(p);
            }

            Point2D[] points = new Point2D[pointsList.size()];
            pointsList.toArray(points);

            // sort points by x values
            Arrays.sort(points, X_COMPARATOR);

            // Make sure there are no duplicates in points[]
            int newSize = removeDuplicates(points, points.length);
            if (newSize < points.length) {
                points = Arrays.copyOfRange(points, 0, newSize);
                Arrays.sort(points, X_COMPARATOR);
            }

            Point2D[] kdTree = new Point2D[(points.length) * 2 + 1];
            buildKDTree(kdTree, points, 0, points.length - 1, 0, Point2D.Dimension.Y);

            /*System.out.println("KDTree of Reducer #" + key + ":"); //TODO
            for (Point2D p: kdTree) {
                System.out.print(p + " ");
            }
            System.out.println(); */

            // Get all the points in the range
            List<Point2D> searchResult = rangeSearch(kdTree, Xmin, Xmax, Ymin, Ymax);

            /*System.out.println("Reducer #" + key + " Search results:"); //TODO
            for (Point2D p : searchResult) {
                output.collect(new Text(key), new Text(p.getX() + "," + p.getY() + " "));
                System.out.print(p.getX() + "," + p.getY() + " ");
            }
            System.out.println(); */
        }


        /*
         * Build KDTree in an array.
         * @param kdTree - array in which KDTree is built
         * @param points - points that used as a values in KDTree
         * @param start - start index in points[]
         * @param end - end index in points[]
         * @param parentIdx - parents index in kdTree[]
         * @param dimension - parent's dimension
         */
        private void buildKDTree(Point2D[] kdTree, Point2D[] points, int start, int end,
                                 int parentIdx, Point2D.Dimension dimension) {

            if (start >= end) { return; }

            Point2D.Dimension dim;
            Comparator<Point2D> comparator;

            // if parent node has x-dimension, then the comparison values for insertion in the BST is
            // x-coordinate values of parent and its children points.
            if (dimension == Point2D.Dimension.X) {
                dim = Point2D.Dimension.Y;
                comparator = Y_COMPARATOR;
            } else {
                // if parents node has y-dimension, then comparison is done
                // by its point and children points y-coordinate values.
                dim = Point2D.Dimension.X;
                comparator = X_COMPARATOR;
            }

            // sort points by corresponding comparator, either by x or y values
            Arrays.sort(points, start, end, comparator);

            /* find median point in the sorted array. This point is the root of KD tree */
            int median = (start + end) / 2;
            //System.out.println("ParenIdx " + parentIdx + " Median " + median);
            points[median].setDimension(dim);
            kdTree[parentIdx] = points[median];

            // left child
            buildKDTree(kdTree, points, start, median,(parentIdx * 2) + 1, dim);
            //right child
            buildKDTree(kdTree, points, median+1, end, (parentIdx * 2) + 2, dim);
        }



        public static final Comparator<Point2D> X_COMPARATOR = (p1, p2) -> {
            int result = Integer.compare(p1.getX(), p2.getX());
            if (result == 0) {
                return Integer.compare(p1.getY(), p2.getY());
            }
            return result;
        };

        public static final Comparator<Point2D> Y_COMPARATOR = (p1, p2) -> {
            int result = Integer.compare(p1.getY(), p2.getY());
            if (result == 0) {
                return Integer.compare(p1.getX(), p2.getX());
            }
            return result;
        };


        /*
         * Retrieves all the points present in the given query rectangle.
         * @param xMin bottom-left x-coordinate of query rectangle's point
         * @param xMax bottom-right x coordinate of query rectangle's point
         * @param yMin bottom-left y-coordinate of query rectangle's point
         * @param yMax top-left y-coordinate of query rectangle's point
         * @return list of points present in the query rectangle
         */
        private List<Point2D> rangeSearch(Point2D[] kdTree, int xMin, int xMax, int yMin, int yMax) {
            List<Point2D> result = new ArrayList<>();
            if (kdTree == null || kdTree.length == 0) { return result; }

            rangeSearchHelper(result, kdTree, 0, xMin, xMax, yMin, yMax);
            return result;
        }

        /*
         * Find all points in a query axis-aligned rectangle.
         * Check if point in node lies in given rectangle.
         * Recursively search left/bottom (if any could fall in rectangle).
         * Recursively search right/top (if any could fall in rectangle).
         */
        private void rangeSearchHelper(List<Point2D> result, Point2D[] kdTree, int parentIdx,
                                       int xMin, int xMax, int yMin, int yMax) {

            if (parentIdx >= kdTree.length || kdTree[parentIdx] == null) { return; }

            int parentXVal = kdTree[parentIdx].getX();
            int parentYVal = kdTree[parentIdx].getY();

            /* If parent point is in the given range add its point to result */
            if ((parentXVal >= xMin && parentXVal <= xMax) &&
                    (parentYVal >= yMin && parentYVal <= yMax)) {
                result.add(kdTree[parentIdx]);
            }

            if (kdTree[parentIdx].getDimension() == Point2D.Dimension.X) {  // skip children with X < xMin and X > xMax
                if (parentXVal >= xMin) {
                    // left child
                    rangeSearchHelper(result, kdTree,parentIdx*2 + 1, xMin, xMax, yMin, yMax);
                }

                if (parentXVal <= xMax) {
                    // right child
                    rangeSearchHelper(result, kdTree, parentIdx*2 + 2, xMin, xMax, yMin, yMax);
                }

            }

            if (kdTree[parentIdx].getDimension() == Point2D.Dimension.Y) {  // skip children with Y < yMin and Y > yMax
                if (parentYVal >= yMin) {
                    //left child
                    rangeSearchHelper(result, kdTree, parentIdx*2 + 1, xMin, xMax, yMin, yMax);
                }

                if (parentYVal <= yMax) {
                    //right child
                    rangeSearchHelper(result, kdTree,parentIdx*2 + 2, xMin, xMax, yMin, yMax);
                }
            }

        }
    }


    public static class ReduceSearchResults extends MapReduceBase implements Reducer<Text, Text, Text, Text> {

        @Override
        public void reduce(Text key, Iterator<Text> values, OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
            List<String> points = new ArrayList<>();

            while (values.hasNext()) {
                points.add(values.next().toString());
            }

            System.out.println("Range Search MR results:");
            for (String p : points) {
                output.collect(new Text(key), new Text(p + " "));
                System.out.println(p);
            }
        }
    }


    /* Function to remove duplicate elements in the given list of Points2D.
     * @returns new size of modified array
     */
    private static int removeDuplicates(Point2D[] points, int size) {
        if (size == 0) { return size; }

        //Assuming all elements in the input array are unique
        int numUniqueElements = points.length;

        for (int i = 0; i < numUniqueElements; i++) {
            for (int j = i + 1; j < numUniqueElements; j++) {
                //if any two elements are found equal
                if(points[i].equals(points[j])) {
                    //replace duplicate element with last unique element
                    points[j] = points[numUniqueElements - 1];

                    numUniqueElements--;
                    j--;
                }
            }
        }
        return numUniqueElements;
    }



    // args len = 9
    // Arguments list format: input output numPoints pointXmin pointXmax Xmin Xmax Ymin Ymax
    //                          0      1       2         3         4       5    6    7    8
    public static void main(String[] args) throws Exception {

        if (args.length < 9) {
            System.err.println("ERROR: passes arguments size must be 5: " +
                    "input output numPoints pointXmin pointXmax Xmin Xmax Ymin Ymax");
        }

        xCoordinateMin = Integer.parseInt(args[3]);
        xCoordinateMax = Integer.parseInt(args[4]);
        Xmin = Integer.parseInt(args[5]);
        Xmax = Integer.parseInt(args[6]);
        Ymin = Integer.parseInt(args[7]);
        Ymax = Integer.parseInt(args[8]);


        //--------------- setting up Job 1 --------------------

        // temp/ will be used by first reducer to collect output
        // of local convex hulls' points
        String tempDir = "temp/";
        JobConf conf1 = new JobConf(RangeSearchMapReduce.class);
        conf1.setJobName("RangeSearch");

        conf1.setOutputKeyClass(Text.class);
        conf1.setOutputValueClass(Text.class);

        conf1.setMapperClass(MapLocalKDTrees.class);
        conf1.setReducerClass(ReduceLocalKDTrees.class);

        conf1.setInputFormat(TextInputFormat.class);
        conf1.setOutputFormat(TextOutputFormat.class);

        FileInputFormat.setInputPaths(conf1, new Path(args[0]));
        FileOutputFormat.setOutputPath(conf1, new Path(tempDir));

        conf1.set("size", args[2]);

        //--------------- setting up Job 2 ---------------------
        JobConf conf2 = new JobConf(RangeSearchMapReduce.class);
        conf2.setJobName("CollectSearchResults");

        conf2.setOutputKeyClass(Text.class);
        conf2.setOutputValueClass(Text.class);

        conf2.setMapperClass(MapResults.class);
        conf2.setReducerClass(ReduceSearchResults.class);

        conf2.setInputFormat(KeyValueTextInputFormat.class);
        conf2.setOutputFormat(TextOutputFormat.class);

        FileInputFormat.setInputPaths(conf2, new Path(tempDir));
        FileOutputFormat.setOutputPath(conf2, new Path(args[1]));

        long startTime = System.nanoTime(); // start timer

        JobClient.runJob(conf1);
        JobClient.runJob(conf2);

        long endTime = System.nanoTime();  // stop timer
        long totalTime = endTime - startTime;

        System.out.println("Elapsed time: " + totalTime / 1000000000 + " seconds");
    }

}
