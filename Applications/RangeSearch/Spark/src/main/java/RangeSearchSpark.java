package edu.uw.bothell.rangesearch.spark;

import java.util.*;
import java.util.ArrayList;
import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;

/**
 * RangeSearchSpark.java
 * Project: RangeSearch
 * University of Washington Bothell, Distributed Systems Laboratory
 * Spring 2020
 * @author Satine Paronyan
 */
public class RangeSearchSpark {

    private static int Xmin = 0;
    private static int Xmax = 0;
    private static int Ymin = 0;
    private static int Ymax = 0;
    private static int totalNumPoints = 0;

    public static void main(String[] args) {

        if (args.length != 7) {
            System.err.println("Wrong number of arguments passed.");
        }

        Xmin = Integer.parseInt(args[2]);
        Xmax = Integer.parseInt(args[3]);
        Ymin = Integer.parseInt(args[4]);
        Ymax = Integer.parseInt(args[5]);
        totalNumPoints = Integer.parseInt(args[6]);

        // Start Spark
        // Read the input file
        String inFile = args[0];
        System.err.println("\nInput file: " + inFile + "\n");
        SparkConf conf = new SparkConf().setAppName("RangeSearchSpark");
        JavaSparkContext javaSparkContext = new JavaSparkContext(conf);

        // pass global variables to spark context
        Broadcast<Integer> xMinimum = javaSparkContext.broadcast(Xmin);
        Broadcast<Integer> xMaximum = javaSparkContext.broadcast(Xmax);
        Broadcast<Integer> yMinimum = javaSparkContext.broadcast(Ymin);
        Broadcast<Integer> yMaximum = javaSparkContext.broadcast(Ymax);

        JavaRDD<String> lines = javaSparkContext.textFile(inFile);

        int slices = Integer.parseInt(args[1]);
        System.err.println("\nNumber of partitions " + slices + "\n");

        long startTime = System.nanoTime(); // start timer

        // Initializing points
        JavaRDD<Point2D> pointsRDD = lines.flatMap(line -> {
            line.trim();

            String[] buffer = line.split(" ");
            List<Point2D> point2DList = new ArrayList<>();

            for (String point: buffer) {
                String[] xy = point.split(",");
                try {
                    point2DList.add(new Point2D(Integer.parseInt(xy[0]), Integer.parseInt(xy[1])));
                } catch (NumberFormatException e) {
                    System.out.println("======== Caught Exception: xy[0]=" + xy[0] + " " + "xy[1]=" + xy[1] + "\n");
                }
            }
            return point2DList.iterator();
        });

        // Split points
        JavaRDD<Point2D> splitPointsRDD = pointsRDD.mapPartitions( iterator -> {
            Xmin = xMinimum.value();
            Xmax = xMaximum.value();
            Ymin = yMinimum.value();
            Ymax = yMaximum.value();

            // build the list of points to be used for range search
            List<Point2D> mySlice = new ArrayList<>();
            while (iterator.hasNext()) {
                Point2D point2D = iterator.next();
                mySlice.add(point2D);
            }

            Point2D[] myPoints = new Point2D[mySlice.size()];
            mySlice.toArray(myPoints);

            // sort points by x values
            Arrays.sort(myPoints, Point2D.X_COMPARATOR);

            // remove duplicate points if any
            int newSize = removeDuplicates(myPoints, myPoints.length);
            if (newSize < myPoints.length) {
                myPoints = Arrays.copyOfRange(myPoints, 0, newSize);
                Arrays.sort(myPoints, Point2D.X_COMPARATOR);
            }

            // Build KD tree
            Point2D[] tree = RangeSearch.kdTree(myPoints);

            // do range search
            Point2D[] search = RangeSearch.search(tree, Xmin, Xmax, Ymin, Ymax);
            List<Point2D> result = Arrays.asList(search);

            return  result.iterator();
        });

        // Get all the search result points
        JavaRDD<Point2D> resultsRDD = splitPointsRDD.coalesce(1);
        JavaRDD<Point2D> result = resultsRDD.map( p -> p);
        List<Point2D> res = result.collect();

        // stop timer
        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;
        System.err.println("\nElapsed time: " + totalTime / 1000000000 + " seconds");
        System.err.println();

        // Output found points
        System.out.println("====== TOTAL INPUT NUMBER OF POINTS: " + totalNumPoints);
        System.out.println("======= Points found in requested range: " + res.size());
        System.out.println("======= Found points in range:\n");
        for (Point2D p : res) {
            System.out.println("" + p.getX() + "," + p.getY());
        }

        // shutdown spark
        javaSparkContext.stop();
    }


    /* Function to remove duplicate elements in the given list of Points2D.
     * @returns new size of modified array
     */
    private static int removeDuplicates(Point2D[] points, int size) {
        if (size == 0) { return size; }

        //Assuming all elements in the input array are unique
        int numUniqueElements = points.length;

        for (int i = 0; i < numUniqueElements; i++) {
            for (int j = i + 1; j < numUniqueElements; j++) {
                //if any two elements are found equal
                if(points[i].equals(points[j])) {
                    //replace duplicate element with last unique element
                    points[j] = points[numUniqueElements - 1];

                    numUniqueElements--;
                    j--;
                }
            }
        }
        return numUniqueElements;
    }
}
