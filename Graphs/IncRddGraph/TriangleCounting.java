import breeze.storage.Storage;
import model.GraphModel;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.graphx.Edge;
import org.apache.spark.graphx.EdgeContext;
import org.apache.spark.graphx.EdgeDirection;
import org.apache.spark.graphx.EdgeRDD;
import org.apache.spark.graphx.EdgeTriplet;
import org.apache.spark.graphx.Graph;
import org.apache.spark.graphx.GraphLoader;
import org.apache.spark.graphx.PartitionStrategy;
import org.apache.spark.graphx.TripletFields;
import org.apache.spark.graphx.VertexRDD;
import org.apache.spark.rdd.RDD;
import org.apache.spark.storage.StorageLevel;
import scala.Function1;
import scala.Function2;
import scala.Function3;
import scala.Option;
import scala.Predef;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.reflect.ClassTag;
import scala.reflect.ClassTag$;
import scala.runtime.BoxedUnit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TriangleCounting {
    public static void main(String [] args) {
        JavaSparkContext sc = new JavaSparkContext("spark://cssmpi1h.uwb.edu:58000", "TriangleCounting");
        // count_triangles_file(sc);
        
        count_triangles_dynamic(sc);
    }
    
    private static void count_triangles_dynamic(JavaSparkContext sc) {
        GraphModel model = generateModel(1000);
        
        Graph<String, String> graph = generateGraph(sc, model);
        
        long begin = System.currentTimeMillis();
        
        scala.Tuple2 [] vertices = (scala.Tuple2[]) graph.vertices().collect();
        org.apache.spark.graphx.Edge [] edges = (org.apache.spark.graphx.Edge []) graph.edges().collect();

        System.out.println("Edge count: " + edges.length);

        long end = System.currentTimeMillis();
        
        long duration = end - begin;

        System.out.println("First pass: " + duration / 1000.0);
        System.out.println("pass" + graph.vertices().getNumPartitions());
        
        graph.cache();
        
        List<String> benchmarkOutput = new ArrayList<>();
        
        for (int i = 0; i < 46; i++) {
            System.out.println("Iteration: " + i);
            
            begin = System.currentTimeMillis();

            // modify
            model = extendModel(model, 100);

            graph = generateGraph(sc, model);

            // Count vertices
            vertices = ((scala.Tuple2[]) graph.vertices().collect());
            edges = (org.apache.spark.graphx.Edge []) graph.edges().collect(); // added line to make program fair
            
            duration = System.currentTimeMillis() - begin;

            // iteration, vertexCount, vertexDelta, runtime
            benchmarkOutput.add(String.format("%d, %d, %d, %f", i, vertices.length, 100, duration / 1000.0));
        }
        
        sc.parallelize(benchmarkOutput).saveAsTextFile("target/output-1000");
    }

    private static Graph<String, String> generateGraph(JavaSparkContext sc, GraphModel model) {
        long begin = System.currentTimeMillis();
        
        Graph<String, String> graph = null;
        
        List<Edge<String>> edges = new ArrayList<>();
        
        List<Tuple2<Object, String>> vertices = model.getVertices().stream()
                .map(v -> {
                    for (Object neighbor : v.neighbors) {
                        long source = (int) v.id, dest = (int) neighbor;
                        
                        edges.add(new Edge(source, dest, ""));
                    }
                  
                    long vid = (int) v.id;
                    
                    return new Tuple2<Object, String>(vid, v.id.toString());
                }).collect(Collectors.toList());
        
        JavaRDD<Tuple2<Object, String>> verticesRDD = sc.parallelize(vertices);
        JavaRDD<Edge<String>> edgesRDD = sc.parallelize(edges);
        
        ClassTag<String> classTag = ClassTag$.MODULE$.apply(String.class);
        
        graph = Graph.apply(verticesRDD.rdd(), edgesRDD.rdd(), "",
                StorageLevel.MEMORY_ONLY(), StorageLevel.MEMORY_ONLY(), classTag, classTag);
        
        long duration = System.currentTimeMillis() - begin;
        
        return graph;
    }

    private static GraphModel generateModel(int nodeCount) {
        GraphModel model = new GraphModel();
        
        for (int i = 0; i < nodeCount; i++) {
            List<Object> neighbors = new ArrayList<>(nodeCount - 1);
            
            for (int n = 0; n < nodeCount; n++) {
                if (n != i) neighbors.add(n);
            }
            
            model.addVertex(i, neighbors);
        }
        
        return model;
    }
    
    private static GraphModel extendModel(GraphModel model, int count) {
        final int size = model.getVertices().size();
        final int start = size;

        for (int i = 0; i < count; i++) {
            List<Object> neighbors = new ArrayList<>(size - 1);

            for (int n = 0; n < size + count; n++) {
                if (n != start + i) neighbors.add(n);
            }

            model.addVertex(start + i, neighbors);
        }
        
        return model;
    }
}
