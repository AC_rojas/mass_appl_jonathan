
/*
 * Incremental graph using IncRDD
 * how to run: spark-submit --num-executors $i --driver-java-options "-Xss100M" --driver-memory 10g --class $1 --conf "spark.executor.extraJavaOptions=-Xss100m" --master spark://cssmpi1h.uwb.edu:58000 $1.jar
 */
// import org.apache.spark.api.java.JavaRDD;
// import org.apache.spark.api.java.function;
// import org.apache.spark.api.java.JavaSparkContext;

import org.apache.spark.api.java.*;
import org.apache.spark.rdd.RDD;

import scala.Tuple2;
import scala.reflect.ClassTag;
import scala.reflect.ClassTag$;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import graphinc.EdgeInc;
import incrdd.IncRDD;       // IncRDD library
import model.GraphModel;    // justins graph model

public class IncGraphCreation {
    public static void main(String [] args) {
        

        JavaSparkContext sc = new JavaSparkContext("spark://cssmpi1h.uwb.edu:58000", "IncGraph");
        count_triangles_dynamic(sc);
    }
    

    private static void count_triangles_dynamic(JavaSparkContext sc) {
        
        GraphModel model = new GraphModel();
        
        model = generateModel(1000);         // genterates graph model 
        
        IncRDD verticesIncRDD = generateGraph(sc, model);                // creates the initial edge and vetices incRDDs
        
        long begin = System.currentTimeMillis();
        
        scala.Tuple2[] vertices = (scala.Tuple2[])verticesIncRDD.collect();
        
        System.out.println("Vertices: " + verticesIncRDD.count());
        
        long end = System.currentTimeMillis();
        
        long duration = end - begin;

        System.out.println("First pass: " + duration / 1000.0);
        //verticesIncRDD.cache();
        
        int curent_size = 1000;         //use instead of vertices length to quicly modify to (no collect)
        
        List<String> benchmarkOutput = new ArrayList<>();
        for (int i = 0; i < 46; i++) {
            begin = System.currentTimeMillis();

            System.out.println("Iteration: " + i);
            
            // modify
            verticesIncRDD = ExtendGraph( curent_size, verticesIncRDD, 100);
            
            vertices = (scala.Tuple2[])verticesIncRDD.collect();
            
            duration = System.currentTimeMillis() - begin;
            curent_size += 100;
            benchmarkOutput.add(String.format("%d, %d, %d, %f", i, curent_size, 100, duration / 1000.0));
        }
            
        sc.parallelize(benchmarkOutput).saveAsTextFile("target/output-1000");
    
    }

    /*
     * ExtendGraph uses the model created and increses the size 
     * of both incrdd graph(vertecies and edges), and the model
     */
    private static IncRDD ExtendGraph(int model, IncRDD verticesIncRDD, int count) {
        final int size = model;
        final int start = size;
        long begin = System.currentTimeMillis();
        
        // loops through 
        for(int i = 0; i < count; i++){
            int id = i + start;
            List<Object> neighbors = new ArrayList<>(size + count - 1);
            
            for(int neighbor = 0;  neighbor < size + count; neighbor++){
                if (neighbor != start){
                    // adds an edge to neighbor class
                    neighbors.add(new EdgeInc(id, neighbor, ""));
                }   
            }
            
            //neighbors.forEach(v -> System.out.println(((EdgeInc)v).dest));
            verticesIncRDD = verticesIncRDD.add((int)id, neighbors);
        }

        // RDD temp = graphInc.verticesIncRDD.toJavaRDD().repartition(graphInc.verticesIncRDD.getNumPartitions()).rdd();
        // ClassTag<Integer> classTag = ClassTag$.MODULE$.apply(Integer.class);
        // ClassTag<List<Object>> classTagList = ClassTag$.MODULE$.apply(List.class);
        // verticesIncRDD = IncRDD.apply( temp, classTag, classTag); //List);
        long duration = System.currentTimeMillis() - begin;
        //System.out.println("\ntime extending: " + duration);
        return verticesIncRDD;
    }

    /*
     * generates the model used to create the initial graph
     */
    private static GraphModel generateModel(int nodeCount) {
        GraphModel model = new GraphModel();
        
        for (int i = 0; i < nodeCount; i++) {
            List<Object> neighbors = new ArrayList<>(nodeCount - 1);
            
            for (int n = 0; n < nodeCount; n++){
                if (n != i) neighbors.add(new EdgeInc(i, n, 0));
            }
            
            model.addVertex(i, neighbors);
        }
        return model;
    }
    

    /*
     * Generates a incRDD of the verices
     * Creates an incRDD of the edges using the graphmodel
     * Adds both to the graphInc class
     */
    private static IncRDD generateGraph(JavaSparkContext sc, GraphModel model) 
    {
        
        long begin = System.currentTimeMillis();
        
        List<Tuple2<Integer, List<Object>>> vertices = model.getVertices().stream()
                .map(v -> {
                    int vid = (int) v.id;
                    
                    //VertexInc temp = new VertexInc(vid, v.neighbors);
                    
                    return new Tuple2<Integer, List<Object>>(vid, v.neighbors);
                }).collect(Collectors.toList());
        
        JavaRDD<Tuple2<Integer, List<Object>>> verticesRDD = sc.parallelize(vertices);
        // JavaRDD<Tuple2<String, Integer>> edgeRDD = sc.parallelize(edgesList).repartition(50);

        ClassTag<Integer> classTag = ClassTag$.MODULE$.apply(Integer.class);
        ClassTag<List<Object>> classTagList = ClassTag$.MODULE$.apply(List.class);
        // ClassTag<String> classTagStr = ClassTag$.MODULE$.apply(String.class);

        IncRDD verticesIncRDD = IncRDD.apply(verticesRDD.rdd(), classTag, classTagList);        
        // graphInc.edgeIncRDD = IncRDD.apply(edgeRDD.rdd(), classTagStr, classTag);
        long duration = System.currentTimeMillis() - begin;
    
        return verticesIncRDD;
    }

}
