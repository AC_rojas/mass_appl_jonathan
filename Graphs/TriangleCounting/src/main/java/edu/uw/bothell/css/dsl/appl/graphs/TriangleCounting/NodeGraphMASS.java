package edu.uw.bothell.css.dsl.appl.graphs.TriangleCounting;

import edu.uw.bothell.css.dsl.MASS.MASSBase;
import edu.uw.bothell.css.dsl.MASS.VertexPlace;

import java.util.Arrays;
import java.util.stream.Collectors;

public class NodeGraphMASS extends VertexPlace {
    // function identifiers
    public static final int init_ = 0;
    public static final int display_ = 1;

    public Object callMethod(int functionId, Object argument) {
        switch (functionId) {
            case init_:
                return init(argument);
            case display_:
                return display();
        }
        return null;
    }

    private Object display() {
        try {
            String neighborString = Arrays.stream(getNeighbors())
                    .map(String::valueOf)
                    .collect(Collectors.joining(", "));

            MASSBase.getLogger().debug(getIndex().toString() + ": " + neighborString);

        } catch (Exception e) {
            MASSBase.getLogger().error("Exception displaying vertex", e);
        }
        return null;
    }

    /**
     * Is the default constructor.
     */
    public NodeGraphMASS() {
        super();
    }

    public NodeGraphMASS(Object arg) {
        super(arg);
    }

    public Object init(Object arg) {
        // My network information
        int nNodes = getSize()[0];
        int nodeId = getIndex()[0];

        // Generate my neighbors and their distances
//        Map myMap = new Map(nNodes, nodeId);
//        neighbors = myMap.neighbors;

        return null;
    }
}
