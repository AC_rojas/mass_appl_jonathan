package edu.uw.bothell.css.dsl.appl.graphs.TriangleCounting;
import edu.uw.bothell.css.dsl.MASS.*;
import edu.uw.bothell.css.dsl.MASS.graph.CytoscapeListener;
import edu.uw.bothell.css.dsl.MASS.graph.Graph;
import edu.uw.bothell.css.dsl.MASS.graph.GraphRequest;
import edu.uw.bothell.css.dsl.MASS.graph.MASSListener;
import edu.uw.bothell.css.dsl.MASS.graph.transport.VertexModel;
import edu.uw.bothell.css.dsl.MASS.logging.LogLevel;
import java.lang.reflect.Array;
import java.util.List;
import java.util.Vector;
import java.util.stream.Collectors;

/**
 * CountTriangles.java - Counts and enumerates all triangles in a given graph
 *
 * @author Munehiro Fukuda <mfukuda@uw.edu>
 * @version 1.0
 * @since October 23, 2017
 */
public class CountTrianglesGraphMASS {
    private static final boolean CYTOSCAPE_LISTENER = true;
    private static int sequentialId = 2;

    /**
     * Counts and enumerates all triangles in a given graph
     *
     * @param args TBD
     */
    public static void main(String[] args) {
        // TODO: This program _appears_ to have 1 invariant:
        //     1. The number of vertices must be correct

        // Read and validate input parameters
        if (args.length != 3) {
            System.err.println("args = " + args.length + " should be 3:"
                    + " graph-filename.csv, nVertices, show-output?");
            System.exit(-1);
        }
        int nVertices = Integer.parseInt(args[1]);

        MASS.setLoggingLevel(LogLevel.ERROR);
        MASS.init(10000000);

        // Create a map
        // Places network = new Places( 1, Node.class.getName( ), null, nNodes );

        Object[] graphArguments = new String[]{ args[0], "graph_w.csv"};
        
        System.out.println("Begin data space generation");
        
        long begin = System.currentTimeMillis();
        
        Places network = new GraphPlaces(1, NodeGraphMASS.class.getName(), (String) graphArguments[0], GraphInputFormat.CSV,
                GraphInitAlgorithm.FULL_LIST, 100, graphArguments);
        
        long end = System.currentTimeMillis();
        
        System.out.println("Import complete\nImport time: " + (end - begin) / 1000.0 + " s");

        MASSListener listener = null;

        if (CYTOSCAPE_LISTENER) {
            listener = new CytoscapeListener((GraphPlaces) network);

            listener.registerProcessor("countTriangles",
                    () -> {
                        run_triangle_counting((GraphPlaces) network);

                        return "Success";
                    });
        }

        run_triangle_counting((GraphPlaces) network);

        if (listener != null) {
            listener.finish();
        }

        System.out.println("Finish MASS");

        MASS.finish();
    }

    public static void run_triangle_counting(GraphPlaces network) {
        boolean show = false; //args[2].equals("y");

        Graph graph = network;

        List<VertexModel> vertices = graph.getGraph().getVertices();

        int nVertices = vertices.size();

        try {

            network.callAll(NodeGraphMASS.init_);

            network.callAll(NodeGraphMASS.display_);

            // Time measurement starts
            System.out.println("Go! Graph");
            long startTime = System.currentTimeMillis();

            // Instantiate an agent at each node
            Agents crawlers = new Agents(sequentialId++, CrawlerGraphMASS.class.getName(), null, network, nVertices);

            // TODO: Restructure to use vertexId more consistently
            // This code was written 0 based with sequential integers
            // To that end graph modifications were hammered into global indices but re-running results in a
            // non zero-based integer sequence

            Integer [] indices = vertices.stream()
                    .map(vertex -> {
                        Object value = MASSBase.distributed_map.get(vertex.id);
                        
                        if (value == null) {
                            value = vertex.id;
                        }
                        
                        return (int) value;
                    } )
                    .collect(Collectors.toList())
                    .toArray((Integer[]) Array.newInstance(Integer.class, 0));

            int[][] itineraries = new int[nVertices][4];
            for (int i = 0; i < nVertices; i++)
                for (int j = 0; j < 4; j++)
                    itineraries[i][j] = (j == 0) ? indices[i] : -1;
            crawlers.callAll(CrawlerGraphMASS.init_, (Object[]) itineraries);

            // For stats
            int totalAgents = 0; // max # agents in computation

            for (int i = 0; i < 3; i++) {
                Object currStep = (Object) (new Integer(i));
                if (show)
                    System.out.println("*** iteration = " + i + "************");

                // STEP 1: have each agent spawn its children and let them
                // choose their next destination
                if (show)
                    System.out.println("... step 1: spawn .....................");
                crawlers.callAll(CrawlerGraphMASS.onArrival_, currStep);
                crawlers.manageAll();

                // STEP 2: have each agent migrate along a different edge
                // emanating from the current node
                // at i == 0, the very first agent hops to the origin
                if (show)
                    System.out.println("... step 2: migration .................");
                crawlers.callAll(CrawlerGraphMASS.departure_, currStep);
                crawlers.manageAll();

                // For stats
                int nAgents = crawlers.nAgents();
                totalAgents += nAgents;
            }
            // Get results
            int nAgents = crawlers.nAgents();
            System.out.println("# triangles = " + nAgents);

            // Time measurement ends
            long elapsedTime = System.currentTimeMillis() - startTime;
            System.out.println("Elapsed time = " + elapsedTime);

            // Stats
            System.out.println("Statistics: total #agents = " + totalAgents);

            // Enumerate all triangles
            if (show) {
                int[][] dummyArgs = new int[nAgents][1];
                Object[] triangles = (Object[]) crawlers.callAll(CrawlerGraphMASS.getTriangle_, (Object[]) dummyArgs);

                for (int i = 0; i < nAgents; i++)
                    System.out.println(i + ": " + (String) triangles[i]);

                network.callAll(NodeGraphMASS.display_);
            }
        } catch (Exception e) {
            System.err.println("Error in execution: " + e.getMessage());

            e.printStackTrace(System.err);
        }
    }
}
