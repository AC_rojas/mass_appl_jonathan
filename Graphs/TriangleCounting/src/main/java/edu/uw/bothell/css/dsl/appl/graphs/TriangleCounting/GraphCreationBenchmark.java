package edu.uw.bothell.css.dsl.appl.graphs.TriangleCounting;

import java.io.FileWriter; 
import java.io.IOException;
import edu.uw.bothell.css.dsl.MASS.GraphPlaces;
import edu.uw.bothell.css.dsl.MASS.MASS;
import edu.uw.bothell.css.dsl.MASS.Place;
import edu.uw.bothell.css.dsl.MASS.Places;
import edu.uw.bothell.css.dsl.MASS.VertexPlace;
import edu.uw.bothell.css.dsl.MASS.graph.transport.GraphModel;
import edu.uw.bothell.css.dsl.MASS.graph.transport.VertexModel;
import edu.uw.bothell.css.dsl.MASS.logging.LogLevel;

import edu.uw.bothell.css.dsl.MASS.*;

import java.util.ArrayList;
import java.util.List;

public class GraphCreationBenchmark {
    private static GraphModel generateModel(int nodeCount) {
        GraphModel model = new GraphModel();

        for (int i = 0; i < nodeCount; i++) {
            List<Object> neighbors = new ArrayList<>(nodeCount - 1);

            for (int n = 0; n < nodeCount; n++) {
                if (n != i) neighbors.add(n);
            }

            model.addVertex(i, neighbors);
        }

        return model;
    }

    private static void extendGraph(GraphPlaces graphPlaces, int count, int sizeGraph ) {
        // GraphModel model = graphPlaces.getGraph();
        final int start = sizeGraph;
        final int size = start;

        for (int i = 0; i < count; i++) {
            graphPlaces.addVertex(start + i);
        }
        
        for (int i = 0; i < count; i++) {
            for (int n = 0; n < size + count; n++) {
                if (n != start + i) graphPlaces.addEdge(start + i, n, 1.0);
            }
        }
    }
    
    public static void main(String [] args) {
        System.out.println("MASS");

        MASS.init(1000000);
         
        MASS.setLoggingLevel(LogLevel.ERROR);
        
        Object[] graphArguments = new String[]{ "graphx.xml", "graphy.xml"};

        GraphPlaces graphPlaces =  new GraphPlaces(0, VertexPlace.class.getName(), (String)graphArguments[0], GraphInputFormat.MATSIM, GraphInitAlgorithm.FULL_LIST, 100, graphArguments);//GraphPlaces(0, VertexPlace.class.getName(), 100);
        System.out.println("done");
        System.out.println("Making empty: " + graphPlaces.getSize()[0]);
        
        GraphModel model1 = generateModel(0);
        graphPlaces.setGraph(model1);
        System.out.println("done making empty");
        populateBaseGraph(graphPlaces, 1000);
        
        long start = System.currentTimeMillis();
        
        System.out.println("Graph size: " + graphPlaces.getGraph().getVertices().size());
        
        long end = System.currentTimeMillis();

        System.out.println("Runtime: " + (end - start) / 1000.0);
     
        GraphModel model = graphPlaces.getGraph();
        
        for (int i = 0; i <= 100; i++) {
            start = System.currentTimeMillis();

            extendGraph(graphPlaces, 100, model.getVertices().size());
            model = graphPlaces.getGraph();
            
            end = System.currentTimeMillis();
            
            System.out.println(String.format("%d, %d, %d, %f", i, model.getVertices().size(), 100, (end - start) / 1000.0));
           
        }
        MASS.finish();
    }

    private static void populateBaseGraph(GraphPlaces graphPlaces, int size) {
        GraphModel model = generateModel(size);
        for (VertexModel vertex : model.getVertices()) {
            graphPlaces.addVertex(vertex.id);
        }
        
        for (VertexModel vertex : model.getVertices()) {
            for (Object neighbor : vertex.neighbors) {
                graphPlaces.addEdge(vertex.id, neighbor, 1.0);
            }
        }
    }
}
