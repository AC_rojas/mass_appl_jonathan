package edu.uw.bothell.rangesearch.mass;

import edu.uw.bothell.css.dsl.MASS.MASS;
import edu.uw.bothell.css.dsl.MASS.*;
import edu.uw.bothell.css.dsl.MASS.logging.LogLevel;
import edu.uw.bothell.css.dsl.MASS.VertexPlace;
import edu.uw.bothell.css.dsl.MASS.graph.transport.GraphModel;

import java.util.*;
import java.util.stream.Collectors;

/**
 * RangeSearchMASS.java
 * Project: RangeSearch
 * University of Washington Bothell, Distributed Systems Laboratory
 * Spring 2020
 * @author Satine Paronyan
 */
public class RangeSearchMASS {

    public static GraphPlaces graphPlaces;
    private static int vertexId = 0;
    private static int totalNumPoints;


    // args: filename Xmin Xmax Ymin Ymax totalNumPoints
    public static void main(String[] args) {
        MASS.init(500000);
        MASS.setLoggingLevel(LogLevel.ERROR);
    
        if (args.length < 5) {
            MASS.getLogger().error("----- Wrong number of arguments passed to main: expected 5");
        }

        int Xmin = Integer.parseInt(args[1]);
        int Xmax = Integer.parseInt(args[2]);
        int Ymin = Integer.parseInt(args[3]);
        int Ymax = Integer.parseInt(args[4]);
        Object[][] searchRange = new Integer[1][4];
        searchRange[0][0] = Xmin;
        searchRange[0][1] = Xmax;
        searchRange[0][2] = Ymin;
        searchRange[0][3] = Ymax;
        totalNumPoints = Integer.parseInt(args[5]);
  
        long startTime = System.nanoTime(); // start timer
        // ======= READ POINTS AND BUILD KD TREE (GRAPH) =========
        String file = args[0];
        InputProcessing input = new InputProcessing();
        input.readInput(file);
        // points read from file
        Point2D[] points = input.getPointsList();
        // sort points by x values
        Arrays.sort(points, X_COMPARATOR);

        int newSize = removeDuplicates(points, points.length);
        System.out.println("size: " + newSize);
        
        if (newSize < points.length) {
            points = Arrays.copyOfRange(points, 0, newSize);
            Arrays.sort(points, X_COMPARATOR);
        }

        RangeSearchMASS kdTreeMASS = new RangeSearchMASS(totalNumPoints, points);
        int rootID = kdTreeMASS.buildKDGraph(points);
        boolean flag = graphPlaces != null;;
        
 
        // =================== DO RANGE SEARCH ====================
        //Agents agents = new Agents(2, RangeSearchAgent.class.getName(), null, graphPlaces, 1);
        Agents agents = new Agents(2, "edu.uw.bothell.rangesearch.mass.RangeSearchAgent", null, graphPlaces, 1);
        ArrayList<Point2D> pointsInRange = new ArrayList<>();
        // vertex destinations where agents are initialized. We start with one agent at root node
        
        int destination = 0; // root vertex id
        agents.callAll(RangeSearchAgent.INIT, destination);
        agents.manageAll();
        agents.callAll(RangeSearchAgent.GET_SEARCHRANGE, searchRange);
        agents.manageAll();
        Object result1 = null;
        boolean needData = true;
        int nAgents = agents.nAgents();
        while(needData){
            Object [] n = new Object[nAgents];
            for(int i = 0; i < nAgents; i++){
                n[i] = searchRange[0];
            }
            // System.out.println("looping");
            result1 = agents.callAll(RangeSearchAgent.DO_SEARCH, n);
            System.out.println("***** Number of initial Agents: " + nAgents);
            agents.manageAll();
            nAgents = agents.nAgents();
            Object[] result = (Object[])result1;
            
            for(int i = 0; i < result.length; i++){
                if(result[i] != null && ((Object[])result[i]).length != 0)
                    pointsInRange.add((Point2D)(((Object[])result[i])[0]));
            }
            if(nAgents == 0)
                break;
        }
        long endTime = System.nanoTime();  // stop timer
        long totalTime = endTime - startTime;
        
        MASS.getLogger().error("***** Elapsed time: " + totalTime / 1000000000 + " seconds");
        System.out.println("***** Elapsed time: " + totalTime / 1000000000 + " seconds");

        for(Point2D point1 : pointsInRange){
            System.out.println(point1.getX() + ":" + point1.getY());
        }

        // Output found points
        MASS.getLogger().debug("====== TOTAL INPUT NUMBER OF POINTS: " + totalNumPoints);
        MASS.getLogger().debug("======= Points found in requested range: " + pointsInRange.size());
        MASS.getLogger().debug("======= Found points in range:\n");

        MASS.finish();
    }


    public static final Comparator<Point2D> X_COMPARATOR = (p1, p2) -> {
        int result = Integer.compare(p1.getX(), p2.getX());
        if (result == 0) {
            return Integer.compare(p1.getY(), p2.getY());
        }
        return result;
    };

    public static final Comparator<Point2D> Y_COMPARATOR = (p1, p2) -> {
        int result = Integer.compare(p1.getY(), p2.getY());
        if (result == 0) {
            return Integer.compare(p1.getX(), p2.getX());
        }
        return result;
    };


    public RangeSearchMASS(int size, Point2D[] points) {
        Object[] graphArguments = new Object[]{ "graphx.xml", "graphy.xml", points};
        graphPlaces = new GraphPlaces(0, "edu.uw.bothell.rangesearch.mass.KDTreeNode", size);// (String)graphArguments[0], GraphInputFormat.MATSIM, GraphInitAlgorithm.FULL_LIST, 100, graphArguments);
        GraphModel temp = new GraphModel();
        graphPlaces.setGraph(temp);
    }

    // Receives list of points that are sorted by X-coordinate then build a KD tree
    public int buildKDGraph(Point2D[] points) {
        int i = 0;
        /* find median point in the sorted array. This point is the root of KD tree */
        int median = points.length / 2;
        
        Point2D point2D = new Point2D(points[median].getX(), points[median].getY(), Point2D.Dimension.X);
        int rootId = graphPlaces.addVertex(vertexId, point2D);  // add a new vertex to graph and initialize with point
        rootId = vertexId;
        vertexId++;
        /* Build left subtree with the points that come before median point (root) */
        int leftChildId = buildKDGraphHelper(points, 0, median, point2D.getDimension(), -1);

        /* Build right subtree with the points that come after median point (root) */
        int rightChildId = buildKDGraphHelper(points, median + 1, points.length , point2D.getDimension(),-1);

        graphPlaces.addEdge(rootId, leftChildId, 0);
        graphPlaces.addEdge(rootId, rightChildId , 0);

        return rootId;
    }


    private int buildKDGraphHelper(Point2D[] points, int start, int end, Point2D.Dimension dimension, int parentID) {
        
        if (start >= end) { return parentID; }

        Point2D.Dimension dim;
        Comparator<Point2D> comparator;

        // if parent node has x-dimension, then the comparison values for insertion in the BST is
        // x-coordinate values of parent and its children points.
        if (dimension == Point2D.Dimension.X) {
            dim = Point2D.Dimension.Y;
            comparator = Y_COMPARATOR;
        } else {
            // if parents node has y-dimension, then comparison is done
            // by its point and children points y-coordinate values.
            dim = Point2D.Dimension.X;
            comparator = X_COMPARATOR;
        }

        // sort points by corresponding comparator, either by x or y values
        Arrays.sort(points, start, end, comparator);

        int median = (start + end) / 2;

        Point2D pointParent = new Point2D(points[median].getX(), points[median].getY(), dim);
        int newParentID = graphPlaces.addVertex(vertexId, pointParent);  //add new vertex and initialize with point
        newParentID = vertexId;
        vertexId++;
        int lefChildId = buildKDGraphHelper(points, start, median, dim, newParentID);
        int rightChildId = buildKDGraphHelper(points, median + 1, end, dim, newParentID);
        
        if(newParentID != lefChildId )
            graphPlaces.addEdge(newParentID, lefChildId, 0);
        if(newParentID != rightChildId)
            graphPlaces.addEdge(newParentID, rightChildId, 0);

        return newParentID;
    }


    /* 
     * Function to remove duplicate elements in the given list of Points2D.
     * Modified to fix best case of O(n^2) removal of duplicates
     * @returns new size of modified array
     */
    private static int removeDuplicates(Point2D[] points, int yMax) {
        if (points.length == 0) { return points.length; }

        //Assuming all elements in the input array are unique
        int numUniqueElements = points.length;
        
        for (int i = 0; i < numUniqueElements; i++) {
            int newY = yMax + 1;            
            for (int j = i + 1; j < numUniqueElements; j++) {
                // sorted points by x if point[j].x is not same then no other is the same
                if(points[i].getX() != points[j].getX()){
                    break;
                }
                //if any two elements are found equal
                if(points[i].equals(points[j])) {
                    //replace duplicate element with last unique element
                    Point2D newPoint = new Point2D(points[i].getX(), newY);
                    points[j] = newPoint;

                    newY++;
                    j--;
                }
            }
        }
        return numUniqueElements;
    }

}
